#!/bin/bash

# Start QEMU with the MINIX image as the root volume, and a second volume for testing
qemu-system-i386 -m 64M -net nic,model=ne2k_pci -net user,hostfwd=tcp::2021-10.0.2.15:21 -hda image/minix.img -hdb image/data.img
