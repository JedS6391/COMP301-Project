# COMP301 - Operating Systems

## Project

For the project, I decided to extend the MINIX file system such that it uses a journal to implement atomic updates.

The description from the project list for this extension follows:

> **Journaling File System:** Some modern file systems (e.g. Linux’s ext3) use a journal to implement atomic updates to the file system. This means that, if a crash occurs, a change to the file system (e.g. deleting a file) will be either totally done (removing the file name from the directory, deallocating the i-node and data blocks), or not done at all. A journal is implemented by writing blocks to be changed to a journal rather than to the main part of the disk. After the journal is complete, a flag is written to the journal and the blocks from the journal are written to the main part of the file system. If a crash occurs before the flag is written, the update is lost but the file system will still be consistent. If a crash occurs after the flag is written but before all the blocks have been copied into the main part of the file system, the system will recopy these blocks into the main part of the file system on reboot.

## Building Report

The report for the project is written in Markdown, and can be converted to a PDF, with the aid of LaTex using the Pandoc tool as follows:

`pandoc -s -o JFS.pdf JFS.md`
