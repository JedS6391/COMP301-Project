#include "queue.h"

#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>

struct queue *new_queue() {
    /* Creates a new empty queue */

    struct queue *q;

    if ((q = malloc(sizeof(struct queue))) == NULL) {
        printf("Failed to malloc new queue");

        /* Trust the caller to check this... */
        return NULL;
    }

    q->head = NULL;
    q->tail = NULL;
    q->size = 0;

    return q;
}

int enqueue_context(struct queue *q, struct journal_context *ctx) {
    /* Adds a context to the queue - usually the queue will
     * contain nodes which all refrence contexts, but it is possible
     * with the implementation to have a list of alternating
     * types. */

    struct node *new_node;

    if ((new_node = malloc(sizeof(struct node))) == NULL) {
        printf("Failed to malloc new node");

        return -1;
    }

    new_node->data.context = ctx;
    new_node->next = NULL;
    new_node->prev = NULL;

    if (q->head == NULL && q->tail == NULL) {
        q->head = new_node;
        q->tail = new_node;
        q->size++;

        return 0;
    }

    q->tail->next = new_node;
    new_node->prev = q->tail;
    q->tail = new_node;
    q->size++;

    return 0;
}

int enqueue_entry(struct queue *q, struct journal_entry *entry) {
    /* See above but for a journal entry */

    struct node *new_node;

    if ((new_node = malloc(sizeof(struct node))) == NULL) {
        printf("Failed to malloc new node");

        return -1;
    }

    new_node->data.entry = entry;
    new_node->next = NULL;
    new_node->prev = NULL;

    if (q->head == NULL && q->tail == NULL) {
        q->head = new_node;
        q->tail = new_node;
        q->size++;

        return 0;
    }

    q->tail->next = new_node;
    new_node->prev = q->tail;
    q->tail = new_node;
    q->size++;

    return 0;
}

int free_queue(struct queue *q) {
    /* Frees each node in the queue. */
    
    struct node *curr;

    while ((curr = q->head) != NULL) {
        q->head = q->head->next;

        free(curr);
    }

    return 0;
}

int remove_tail(struct queue *q) {
    /* Removes the tail item from the queue (i.e. the most
     * recently added item). This is not generally included
     * with queues but is helpful for our use case instead of
     * searching the queue. */

    struct node *curr_tail;

    if (q->tail == NULL) {
        /* Nothing to remove */
        return 0;
    }

    curr_tail = q->tail;

    if (curr_tail->prev == NULL) {
        /* No previous element (i.e. towards head of queue).
         * This is a base case where the list has one element.*/

        /* Head and tail will we be pointing to the same
         * node so we could do this in either order */

        q->head = NULL;
        q->tail = NULL;
        free(curr_tail);
        curr_tail = NULL;
        q->size--;

        return 0;
    }

    /* There are some elements further along in the queue */
    q->tail = curr_tail->prev;
    q->tail->next = NULL;

    free(curr_tail);
    curr_tail = NULL;
    q->size--;

    return 0;
}
