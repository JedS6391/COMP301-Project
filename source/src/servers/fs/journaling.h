#include <journal.h>

/* Prototypes
 *
 * Each of these prototypes is implemented in a corresponding
 * file, and there is extensive documentation where the method
 * is defined.
 */

/* Initialisation methods */
int init_journal(dev_t dev, ino_t inode_num);
int deinit_journal(dev_t dev);
int load_journal_header(dev_t dev, ino_t inode_num);

/* Begin/end transaction methods */
struct j_handle *new_transaction(dev_t dev, int space_required);
int end_transaction(struct j_handle *jh);
struct j_handle *new_handle(int available);

/* Context handling methods */
struct journal_context *journal_new_context(void);
int journal_commit(void);
int journal_flush_context(void);
journal_transaction_t *build_transaction(void);

/* Journaling methods */
int journal_update_block(struct j_handle *jh, journal_entry_t *entry);
journal_entry_t *journal_register_intent(struct j_handle *jh, dev_t dev, block_t block_number, int flag);
int journal_deregister_intent(struct j_handle *jh, journal_entry_t *entry);
journal_entry_t *journal_new_block(struct j_handle *jh, struct inode *rip, off_t position);

/* Flusing methods */
int journal_flush_transaction_block(void);
int journal_flush_update_blocks(void);
int journal_flush_commit_block(void);
int journal_flush_header(void);
block_t journal_alloc_block_number(void);
int journal_checkpoint(void);

/* Replay methods */
int journal_replay(void);

/* Helpers */
u32_t checksum(char *data);
u32_t crc32(char *data);

/* Useful helper for printing a message and setting an error no */
void error(char *msg, journal_error_no err);
void debug(char *msg);

/* Global state that is maintained when journaling */
EXTERN struct j_handle *h;

EXTERN journal_t *journal;

/* For efficiency, we load all the block numbers that belong to the journal
 * into memory when we first load the journal. */
EXTERN block_t *journal_block_table;

EXTERN journal_error_no error_n;
