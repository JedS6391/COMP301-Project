#include "fs.h"
#include "buf.h"
#include "inode.h"
#include "super.h"
#include "journaling.h"
#include "queue.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <minix/com.h>

int journal_checkpoint(void) {
    /**
     * Responsible for creating a checkpoint in the journal.
     *
     * A checkpoint basically works as follows:
     *   - When a context uses all of its buffer space, it is committed
     *     which involves writing the context to the journal.
     *   - Any blocks locked by that context are not unlocked, and hence
     *     are not written until the main part of the filesystem until
     *     they are unlocked.
     *   - A checkpoint goes through the committed contexts, and frees
     *     any locked blocks, returning them to the file system. This
     *     means the context in the journal is no longer needed and that
     *     the space used by it can be reclaimed.
     */

    struct node *blk;
    struct node *curr;
    struct journal_context *ctx;
    struct buf *bp;

    DEBUG(("[JFS] Number of commits that will be checkpointed: %d\n", journal->committed->size));

    /* Loop over each committed context that needs to be checkpointed */
    TAILQ_REVERSE_ENTRY_ITERATOR(curr, journal->committed) {
        ctx = curr->q_ctx;

        /* Put back all locked blocks for this context */
        TAILQ_REVERSE_ENTRY_ITERATOR(blk, ctx->locked) {
            /* Get the original buffer pointer. */
            bp = blk->q_entry->data;

            put_block(bp, INODE_BLOCK);

            /* Free the queue nodes data */
            free(blk->q_entry);
            blk->q_entry = NULL;
        }

        /* Move tail along for this context */
        DEBUG(("[JFS] Moving tail from %d to %d\n", journal->header->tail, journal->header->tail + ctx->total_used));
        DEBUG(("[JFS] Head is at %d\n", journal->header->head));

        /* Move the tail pointer along by the size of the context, so that
         * the space in the journal will be reclaimed. */
        journal->header->tail += ctx->total_used;

        DEBUG(("[JFS] Tail now at %d\n", journal->header->tail));

        /* Rotate back to start if necessary */
        if (journal->header->tail >= journal->header->last) {
            journal->header->tail = journal->header->first + (journal->header->tail - journal->header->last);
        }

        /* We've free'd some number of slots in the journal
         * so we can reclaim them for further contexts */
        journal->free += ctx->total_used;

        /* Write the header to disk */
        journal_flush_header();

        /* Free any data structures, etc. used by the context being checkpointed.
         * This step is very important, otherwise we will have a memory leak and
         * the system will use more and memory until the FS runs out of memory
         * (and as a result will make bad things happen). We trust that any committing
         * code will responsibly tidy up any of the blocks from their queues, etc.
         * so we can just free the queues themselves, and the actual context. */

        free_queue(ctx->locked);
        free(ctx->locked);
        ctx->locked = NULL;

        /* Sanity check */
        if (ctx->transaction != NULL || ctx->commit != NULL ||
            ctx->updates != NULL || ctx->locked != NULL) {

            /* Really helpful message - what else can be done. */
            debug("A part of the context was not free'd");

            return -1;
        }

        /* Free this context as we don't need to care about it anymore */
        free(ctx);
        ctx = NULL;

    }

    /* Make sure to free the old committed queue as it *should* now
     * be empty and we can start with a new one */
    free_queue(journal->committed);
    free(journal->committed);
    journal->committed = NULL;
    journal->committed = new_queue();

    return 0;
}

int journal_commit(void) {
    /**
     * Commits the current context to the journal.
     *
     * This is a multi-step process that involves:
     *   - Creating the commit block
     *   - Flushing the context to disk (comprised of more steps)
     *   - Flusing the journal header
     */

    journal_commit_t *commit;

    /* Allocate commit block for this context. */
    if ((commit = malloc(sizeof(journal_commit_t))) == NULL) {
        debug("Failed to malloc new commit");

        return -1;
    }

    memset(commit, 0, sizeof(journal_commit_t));

    /* Create commit block */
    commit->commit_id = JOURNAL_COMMIT_MAGIC;
    commit->checksum = 0;
    commit->sequence_n = journal->context->transaction->sequence_n;
    journal->context->commit = commit;

    DEBUG(("Built commit: seq = %d\n", commit->sequence_n));

    /* Add to list of committed (but not checkpointed) contexts. */
    enqueue_context(journal->committed, journal->context);

    /* Begin flushing this context to disk. */
    journal_flush_context();

    /* Write the header to disk */
    journal_flush_header();

    /* Get ready for new context to be created */
    journal->context = NULL;

    return 0;
}

u32_t crc32(char *data) {
    /* Implementation adapted from: http://www.hackersdelight.org/hdcodetxt/crc.c.txt */

   int i, j;
   unsigned int byte, crc, mask;
   static unsigned int table[256];

   /* Set up the table, if necessary. */
   if (table[1] == 0) {
      for (byte = 0; byte <= 255; byte++) {

         crc = byte;

         for (j = 7; j >= 0; j--) {    /* Do eight times. */
            mask = -(crc & 1);
            crc = (crc >> 1) ^ (0xEDB88320 & mask);
         }

         table[byte] = crc;
      }
   }

   /* Through with table setup, now calculate the CRC. */
   i = 0;
   crc = 0xFFFFFFFF;

   while ((byte = data[i]) != 0) {
      crc = (crc >> 8) ^ table[(crc ^ byte) & 0xFF];
      i = i + 1;
   }

   return ~crc;
}

int journal_flush_header(void) {
    /**
     * Flushes the journal header to disk so that the
     * state of the journal is up to date.
     */

    void *buf;
    journal_block_header_t *header_block;
    block_t block_number;
    off_t pos;

    /* Get the header block, it's block number and calculate the offset. */
    header_block = journal->header;
    block_number = journal_block_table[0];
    pos = (off_t) block_number * journal->header->block_size;

    if ((buf = malloc(journal->header->block_size)) == NULL) {
        debug("Failed to malloc buffer for writing transaction block");

        return -1;
    }

    memset(buf, 0, journal->header->block_size);
    memcpy(buf, header_block, sizeof(journal_block_header_t));

    if (dev_io(DEV_WRITE, journal->dev->dev, FS_PROC_NR, buf, pos, journal->header->block_size, 0) != journal->header->block_size) {
        /* Something wrong? */

        debug("Failed to write buffer to device");

        return -1;
    }

    free(buf);
    buf = NULL;

    return 0;

}

int journal_flush_context(void) {
    /**
     * Flushes the current context to disk as part of the commit operation.
     */

    /* Firstly, flush transaction block */
    if (journal_flush_transaction_block() != 0) {
        debug("Error flushing journal transaction block");

        return -1;
    }

    /* Then the updates */
    if (journal_flush_update_blocks() != 0) {
        debug("Error flushing journal update blocks");

        return -1;
    }

    /* And finally the commit block */
    if (journal_flush_commit_block() != 0) {
        debug("Error flushing journal commit block");

        return -1;
    }

    return 0;
}

int journal_flush_transaction_block(void) {
    /**
     * Flushes the transaction block of the current context to
     * the journal on disk.
     */

    void *buf;
    journal_transaction_t *transaction_block;
    block_t block_number;
    off_t pos;

    transaction_block = journal->context->transaction;

    /* Compute a checksum of the blocks bytes */
    transaction_block->checksum = crc32((char *) transaction_block);

    journal->header->first_sequence_n = transaction_block->sequence_n;

    /* Get a new block number and calculate the offset on disk. */
    block_number = journal_alloc_block_number();
    pos = (off_t) block_number * journal->header->block_size;

    if ((buf = malloc(journal->header->block_size)) == NULL) {
        debug("Failed to malloc buffer for writing transaction block");

        return -1;
    }

    memset(buf, 0, journal->header->block_size);
    memcpy(buf, transaction_block, sizeof(journal_transaction_t));

    /* Do the write. */
    if (dev_io(DEV_WRITE, journal->dev->dev, FS_PROC_NR, buf, pos, journal->header->block_size, 0) != journal->header->block_size) {
        /* Something wrong? */

        debug("Failed to write buffer to device");

        return -1;
    }

    free(buf);
    free(journal->context->transaction);
    buf = NULL;
    journal->context->transaction = NULL;

    return 0;

}

int journal_flush_update_blocks(void) {
    /**
     * Flushes the update meta block and update blocks to the journal.
     *
     * The update blocks are flushed before the update meta block is,
     * but the meta block logically comes before the update blocks.
     * This is done by allocating the block number for the update meta
     * first so that any subsequent updates come in block numbers
     * after that.
     */

    void *buf;
    block_t block_number;
    off_t pos;
    struct node *entry;
    int run = 0, checksum = 0;
    size_t run_size;
    int i;

    /* Meta block describing the collection of updates that come next */
    journal_update_meta_t *update_meta_block;

    /* An actual data block */
    struct buf *update_data;

    /* First we need to create a metadata block for this update */
    if ((buf = malloc(journal->header->block_size)) == NULL) {
        debug("Failed to malloc buffer for writing update block");

        return -1;
    }

    /* Allocate space for the update meta struct and some amount for the block run
     * together so that they are in memory together and can be written out together */
    if ((update_meta_block = malloc(sizeof(journal_update_meta_t))) == NULL) {
        debug("Failed to malloc update meta block");

        return -1;
    }

    memset(update_meta_block, 0, sizeof(journal_update_meta_t));
    memset(buf, 0, journal->header->block_size);

    /* Allocate a block number for the meta block */
    block_number = journal_alloc_block_number();

    update_meta_block->update_meta_id = JOURNAL_UPDATE_MAGIC;
    update_meta_block->number_updates = journal->context->updates->size;
    update_meta_block->checksum = 0;
    update_meta_block->block_number = block_number;

    /* Start writing the updates at the next block */
    block_number = journal_alloc_block_number();

    TAILQ_REVERSE_ENTRY_ITERATOR(entry, journal->context->updates) {
        /* Get the address of the actual data portion of this update */
        update_data = &(entry->q_entry->copy);

        /* Where this block goes */
        pos = (off_t) block_number * journal->header->block_size;

        if (dev_io(DEV_WRITE, journal->dev->dev, FS_PROC_NR, update_data, pos, journal->header->block_size, 0) != journal->header->block_size) {
            /* Something wrong? */

            debug("Failed to write buffer to device");

            return -1;
        }

        /* Added a block */
        update_meta_block->block_run[run++] = entry->q_entry->block_number;
        update_meta_block->checksums[checksum++] = crc32((char *) &entry->q_entry->copy);

        /* Move to next block if not done. We are done  */
        if (entry != journal->context->updates->tail) {
            block_number = journal_alloc_block_number();
        }

        free(entry->q_entry);
        entry->q_entry = NULL;
    }

    /* We're done with the update queue now so we can free it */
    free_queue(journal->context->updates);
    free(journal->context->updates);
    journal->context->updates = NULL;

    /* Compute a checksum of the blocks bytes */
    update_meta_block->checksum = crc32((char *) update_meta_block);

    /* Finished writing updates - now write update meta block */
    memcpy(buf, update_meta_block, sizeof(journal_update_meta_t));

    pos = (off_t) update_meta_block->block_number * journal->header->block_size;

    if (dev_io(DEV_WRITE, journal->dev->dev, FS_PROC_NR, buf, pos, journal->header->block_size, 0) != journal->header->block_size) {
        /* Something wrong? */

        debug("Failed to write buffer to device");

        return -1;
   }

   free(buf);
   free(update_meta_block);
   buf = NULL;
   update_meta_block = NULL;

    return 0;

}

int journal_flush_commit_block(void) {
    /* Flushes the commit block to the journal. */

    void *buf;
    journal_commit_t *commit_block;
    block_t block_number;
    off_t pos;

    commit_block = journal->context->commit;
    commit_block->checksum = crc32((char *) commit_block);
    block_number = journal_alloc_block_number();
    pos = (off_t) block_number * journal->header->block_size;

    if ((buf = malloc(journal->header->block_size)) == NULL) {
        debug("Failed to malloc buffer for writing commit block");

        return -1;
    }

    memset(buf, 0, journal->header->block_size);
    memcpy(buf, commit_block, sizeof(journal_commit_t));

    if (dev_io(DEV_WRITE, journal->dev->dev, FS_PROC_NR, buf, pos, journal->header->block_size, 0) != journal->header->block_size) {
        /* Something wrong? */

        debug("Failed to write buffer to device");

        return -1;
    }

    free(buf);
    free(journal->context->commit);
    journal->context->commit = NULL;
    buf = NULL;

    return 0;
}

block_t journal_alloc_block_number(void) {
    /**
     * Gives the next available block number in the journal,
     * provided that there is free space in the journal.
     */

    block_t next_block;

    if (journal->free <= 0) {
        /* No blocks available */
        debug("Unable to allocate new block as the journal is full");

        return -1;
    }

    next_block = journal_block_table[journal->header->head];
    journal->header->head++;
    journal->free--;
    journal->context->total_used++;

    /* Cycle the marker if necessary */
    if (journal->header->head >= journal->header->last) {
        journal->header->head = journal->header->first;
    }

    return next_block;
}
