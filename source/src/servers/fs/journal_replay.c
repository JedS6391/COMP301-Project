#include "fs.h"
#include "buf.h"
#include "inode.h"
#include "super.h"
#include "journaling.h"
#include "queue.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <minix/com.h>

int journal_replay(void) {
    /**
     * Replays a portion of the journal that describes changes
     * that need to be done to bring the file system to a consistent
     * state. This is essentially the point between the last checkpoint
     * (i.e. tail) and the last commit (i.e. head).
     */

    /* To replay the journal we loop over the commits
     * that were not checkpointed and rebuild the contexts
     * that they describe. We don't really need to fully
     * build the context, just read the blocks that would
     * make it up. */

    /* TODO: Add magic number checking. */

    /* Markers */
    block_t head;
    block_t tail;

    /* Used to calculate offsets. */
    block_t block_number;
    off_t pos;

    /* These are the blocks that we actually need to read/
     * load into memory. */
    void *update_meta_block_buf;
    void *update_block_buf;
    journal_update_meta_t *update_meta_block;
    int i;

    head = journal->header->head;
    tail = journal->header->tail;
    block_number = journal_block_table[tail];

    /* Starting at the tail marker, increment through
     * the journal until we reach the latest commit block
     * (i.e. head). */
    while (tail < head) {
        /* First, read transaction block (the start of context marker) */
        DEBUG(("[JFS] Reading transaction block at: %d\n", block_number));

        /* Move along */
        tail++;
        block_number = journal_block_table[tail];

        DEBUG(("[JFS] Reading update meta block block at: %d\n", block_number));

        /* Read update meta block */
        pos = (off_t) block_number * journal->header->block_size;

        if ((update_meta_block_buf = malloc(journal->header->block_size)) == NULL) {
            debug("Failed to malloc buffer for reading update metadata block");

            return -1;
        }

        memset(update_meta_block_buf, 0, journal->header->block_size);

        if (dev_io(DEV_READ, journal->dev->dev, FS_PROC_NR, update_meta_block_buf, pos, journal->header->block_size, 0) != journal->header->block_size) {
            /* Something wrong? */

            debug("Failed to read into buffer from device");

            return -1;
        }

        /* Should have a valid update meta block - move to first update. */
        update_meta_block = (journal_update_meta_t *) update_meta_block_buf;
        tail++;

        /* Make sure the checksum is valid. */
        if (update_meta_block->checksum != crc32((char *) update_meta_block)) {
            DEBUG(("[JFS] Invalid update meta block checksum - expected: 0x%x actual: 0x%x\n",
                   update_meta_block->checksum,
                   crc32((char *) update_meta_block)));

            /* Probably best not to continue if things are corrupted. */
            return -1;
        }

        /* Go through the update blocks for this context */
        for (i = 0; i < update_meta_block->number_updates; i++) {
            /* Get the block number for the update */
            block_number = journal_block_table[tail];

            DEBUG(("[JFS] Reading update block at: %d\n", block_number));

            pos = (off_t) block_number * journal->header->block_size;

            if ((update_block_buf = malloc(journal->header->block_size)) == NULL) {
                debug("Failed to malloc buffer for reading update block");

                return -1;
            }

            memset(update_block_buf, 0, journal->header->block_size);

            /* Read the block from the device. */
            if (dev_io(DEV_READ, journal->dev->dev, FS_PROC_NR, update_block_buf, pos, journal->header->block_size, 0) != journal->header->block_size) {
                /* Something wrong? */

                debug("Failed to read into buffer from device");

                return -1;
            }

            if (update_meta_block->checksums[i] != crc32((char *) update_block_buf)) {
                DEBUG(("[JFS] Invalid update block checksum - expected: 0x%x actual: 0x%x\n",
                       update_meta_block->checksums[i],
                       crc32((char *) update_block_buf)));

                /* Don't continue with the replay if we've got a corrupted update. */
                /* TODO: Alternatively, could continue but just skip this update. */
                return -1;
            }

            /* Calculate original block position... */
            pos = update_meta_block->block_run[i] * journal->header->block_size;

            /* ... And copy block to position. */
            if (dev_io(DEV_WRITE, journal->dev->dev, FS_PROC_NR, update_block_buf, pos, journal->header->block_size, 0) != journal->header->block_size) {
                /* Something wrong? */

                debug("Failed to write buffer to device");

                return -1;
            }

            /* Move past this update. */
            tail++;

            /* Done with the buffer so free the memory it was allocated. */
            free(update_block_buf);
            update_block_buf = NULL;
        }

        /* Finally, read commit block */
        block_number = journal_block_table[tail];

        DEBUG(("[JFS] Reading commit block at: %d\n", block_number));

        /* Don't need the meta block anymore */
        free(update_meta_block_buf);
        update_meta_block_buf = NULL;

        tail++;
    }

    /* Update journal header */
    journal->header->tail = tail;

    /* And write it to the disk. */
    journal_flush_header();

    return 0;
}
