#include "fs.h"
#include "buf.h"
#include "inode.h"
#include "super.h"
#include "journaling.h"
#include "queue.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <minix/com.h>

/* Methods for initialisation of a JFS */

int check_journal_inode(struct inode *i);

int init_journal(dev_t dev, ino_t inode_num) {
    /**
     * Initialises the journal by attempting to load the journal
     * header from the device given. The i-node number refers to
     * the journal i-node which is described in the superblock of
     * a device which contains a JFS.
     *
     * The caller of `init_journal` should check that the device
     * contains a JFS first by checking that that `s_magic` field in
     * the superblock matches `SUPER_JOURNAL`.
     *
     * If the journal load is successful then the global journal
     * state will be created and information about the device and
     * journal attached to it, ready for creating transactions.
     */

    /* If a journal has already been loaded, prevent loading another.
     * This prevents more than one JFS being mounted at once.
     * TODO: Allow for multiple JFS devices to be mounted.
     */
    if (journal != NULL && journal->header != NULL) {
        error("A journal is already loaded - aborting journal initialisation to prevent overwriting.",
              JOURNAL_ERROR_INIT);

        return -1;
    }

    /* Attempt to load the journal header - as a side-effect some validity checks are done */
    if (load_journal_header(dev, inode_num) == 0) {
        debug("Successfully loaded (valid) journal header");

        return 0;
    }

    /* Something went wrong - lower layers should have set error_n appropriately */
    return -1;
}

int deinit_journal(dev_t dev) {

    if (journal->dev->dev != dev) {
        /* Trying to deinitialise the journal of a device which is not
         * being journaled */
        error("Can't de-initialise journal as device is not being journaled",
              JOURNAL_INVALID_DEVICE);

        return -1;
    }

    /* Force commit if there is an outstanding context */
    if (journal->context != NULL) {
        journal_commit();
    }

    /* Force checkpoint if there are commits waiting for checkpoint */
    if (journal->committed->size > 0) {
        journal_checkpoint();
    }

    /* Clear up everything that the journal was using.
     * When things are actually being sent to the journal
     * on disk we will need to check for any unwritten data
     * and the likes...
     */
    journal->header = NULL;
    journal->context = NULL;
    journal->dev = NULL;
    journal->committed = NULL;

    free(journal);
    journal = NULL;

    debug("Journal successfully deinitialised");

    return 0;
}

int load_journal_header(dev_t dev, ino_t inode_num) {
    /**
     * Loads the journal header from the given device into
     * a global variable in the context of the FS.
     */

    struct inode *journal_inode;
    block_t header_block_number;
    journal_block_header_t *journal_block_header;
    struct buf *bp;
    journaled_dev_t *journaled_dev;
    int i;

    /* Get the i-node for the journal.
     * We can access it directly as we store the i-node
     * number of the journal in the devices superblock,
     * which we assume is where the information comes from
     * (i.e. we have loaded the superblock).
     */
    journal_inode = get_inode(dev, inode_num);

    /* Sanity-check: we should have a journal i-node if this
     * is a valid JFS and the mode should match what it is
     * set to with mkjfs (mode=regular, user_id=0, group_id=0).
     */
    if (journal_inode == NULL || check_journal_inode(journal_inode)) {
        error("Journal i-node either doesn't exist or is not valid",
              JOURNAL_INVALID_INODE);

        if (journal_inode != NULL) put_inode(journal_inode);

        return -1;
    }

    /* The journal header is the first block for the i-node */
    header_block_number = read_map(journal_inode, 0);

    bp = get_block(dev, header_block_number, NORMAL);

    if ((journal_block_header = malloc(sizeof(journal_block_header_t))) == NULL) {
        debug("Failed to malloc journal_block_header");

        put_inode(journal_inode);

        return -1;
    }

    memset(journal_block_header, 0, sizeof(journal_block_header_t));
    memcpy(journal_block_header, bp->b_data, sizeof(journal_block_header_t));

    put_block(bp, INODE_BLOCK);

    /* Check header is valid */
    if (journal_block_header->journal_id != JOURNAL_ID) {
        error("Loaded journal header ID is not valid - expected: %d, got: %d",
              JOURNAL_INVALID_HEADER);

        free(journal_block_header);
        journal_block_header = NULL;

        put_inode(journal_inode);

        return -1;
    }

    /* Journal header was valid - keep a track of the information
       in the global journal structure. */

    debug("Valid journal header - Initialising global journal state");

    /* Allocate it first */
    if ((journal = malloc(sizeof(journal_t))) == NULL) {
        debug("Failed to malloc new journal");

        put_inode(journal_inode);

        return -1;
    }

    memset(journal, 0, sizeof(journal_t));

    if ((journaled_dev = malloc(sizeof(journaled_dev_t))) == NULL) {
        debug("Failed to malloc device container");

        free(journal);
        journal = NULL;
        put_inode(journal_inode);

        return -1;
    }

    memset(journaled_dev, 0, sizeof(journaled_dev_t));

    journaled_dev->dev = dev;
    journal->dev = journaled_dev;

    if ((journal_block_table = malloc(journal_block_header->total_blocks * sizeof(block_t))) == NULL) {
        debug("Failed to malloc journal block table");

        free(journal);
        journal = NULL;
        put_inode(journal_inode);

        return -1;
    }

    for (i = 0; i < journal_block_header->total_blocks; i++) {
        journal_block_table[i] = read_map(journal_inode, i * journal_block_header->block_size);
    }

    journal->committed = new_queue();

    journal->header = journal_block_header;

    /* Determine whether a replay needs to take place */
    if (journal->header->head != journal->header->tail) {
        debug("Journal replay required: head and tail are not at the same position");

        journal_replay();
    }

    journal->free = journal->header->total_blocks - journal->header->head;

    /* No transaction context for now - another method will do this */
    journal->context = NULL;

    put_inode(journal_inode);

    return 0;
}

int check_journal_inode(struct inode *i) {
    /* Journal i-node should always be the same as how it is
     * set by mkjfs. */

    return i->i_mode != I_REGULAR || i->i_uid != 0 || i->i_gid != 0;
}
