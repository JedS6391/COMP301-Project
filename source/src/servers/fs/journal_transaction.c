#include "fs.h"
#include "buf.h"
#include "inode.h"
#include "super.h"
#include "journaling.h"
#include "queue.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <minix/com.h>

/* Actually declare any extern globals */
struct j_handle *h = NULL;
journal_t *journal = NULL;
block_t *journal_block_table = NULL;
journal_error_no error_n = -1;

/* Helper to output errors if we wish to see them */
void error(char *msg, journal_error_no err) {
    error_n = err;

    if (JOURNAL_LEVEL > JOURNAL_ERROR) {
        printf("[JFS] %s: error_n = %d\n", msg, error_n);
    }
}

/* Helper to output debug messages if we wish to see them. */
void debug(char *msg) {
    if (JOURNAL_LEVEL > JOURNAL_QUIET) {
        printf("[JFS] %s\n", msg);
    }
}

struct j_handle *new_transaction(dev_t dev, int space_required) {
    /**
     * Responsible for allocating transaction contexts
     * to callers who wish to do some journaling.
     *
     * Provided that a new transaction request is made on a
     * valid JFS, the following questions are asked:
     *
     * (1) Is there an existing context?
     *   - If no, allocate a new context if there is space available in
     *      the journal and return a handle for the new context.
     *   - If yes, goto (2).
     * (2) Is there enough space in the current context to satisfy
     *     the request?
     *   - If yes, return a handle for the current context.
     *   - If no, force a commit and allocate a new context if able to.
     *
     * There are some additional side-effects in that if there are a number
     * of outstanding contexts (i.e. those that have been committed but not
     * checkpointed), then a checkpoint may occur. Futhermore, if there is
     * no space in the journal an error will occur in the current implementation.
     * It is hoped that this state would not be reached, but a fix could be to
     * force a commit if the journal becomes full (allowing for space in the
     * journal to be reclaimed).
     */

    /* Sanity checks */
    if (journal == NULL) {
        error("No journal is currently loaded - unable to create transaction",
              JOURNAL_ERROR_NO_LOADED_JOURNAL);

        return NULL;
    }
    else if (journal->dev->dev != dev) {
        error("Unable to create transaction as the current device does not contain a JFS",
              JOURNAL_INVALID_DEVICE);

        return NULL;
    } else if (journal->header == NULL) {
        error("Cannot start new transaction without the loading journal first",
              JOURNAL_ERROR_NO_LOADED_JOURNAL);

        return NULL;
    } else if (h != NULL) {
        /* Shouldn't be an existing handle */
        error("A handle for the transaction context already exists",
              JOURNAL_HANDLE_EXISTS);

        return NULL;
    }

    /* I like to think of the process below as a "context shift" as we may
     * move from one context to another depending on the state of the current
     * context - I found it best to keep myself amused when writing operating
     * systems code as an attempt to lessen the pain... */

    /* Is there an existing context? */
    if (journal->context != NULL) {
        /* Is there space in the existing context to fulfill the request? */
        if (journal->context->used + space_required < journal->context->buffer_size) {
            /* Enough space in current context */
            debug("Using existing context for transaction request");

            return new_handle(space_required);
        } else {
            /* Force a commit and proceed to allocate a new context. */
            debug("Freeing transaction context");

            journal_commit();
        }
    }

    /* Is there enough space in the journal for a new context? */
    if (journal->free < JOURNAL_DEFAULT_CONTEXT_SIZE) {
        error("Not enough space in journal (on disk) for new transaction context",
              JOURNAL_NOT_ENOUGH_SPACE);

        /* TODO: Force a checkpoint here too? */

        return NULL;
    }

    /* When is best to enforce a checkpoint? */
    if (journal->committed->size >= 2) {
        debug("Enforcing checkpoint of committed contexts");

        journal_checkpoint();
    }

    debug("Creating new context");

    /* Allocate a new context and create the transaction
     * block for it. */
    journal->context = journal_new_context();
    journal->context->transaction = build_transaction();

    return new_handle(space_required);
}

struct j_handle *new_handle(int available) {
    /**
     * A handle is required to perform any journaling
     * update operations.
     *
     * The available space just sets a limit on how
     * many updates can be made through this handle
     * before it expires.
     */

    struct j_handle *jh;

    debug("Creating handle for current context");

    if ((jh = malloc(sizeof(struct j_handle))) == NULL) {
        debug("Failed to malloc new handle");

        return NULL;
    }

    /* Clear the memory allocated for this new handle. */
    memset(jh, 0, sizeof(struct j_handle));

    jh->ctx = journal->context;
    jh->used = 0;
    jh->available = available;
    jh->jdev = journal->dev->dev;

    return jh;
}

int end_transaction(struct j_handle *jh) {
    /**
     * Allows a caller to signal the end of a transaction which
     * involves updating the context to reflect how many updates
     * were made through the handle given at the end of transaction.
     */

    if (jh == NULL) {
        /* Can't end a transaction if there is not a valid handle. */
        return -1;
    }

    debug("End of transaction");

    /* Update the amount of buffer used by the handle we're closing */
    journal->context->used += jh->used;

    free(jh);
    jh = NULL;
    h = NULL;

    return 0;
}

struct journal_context *journal_new_context(void) {
    /**
     * Allocates a new context with completely empty state.
     *
     * A context is required to do any journaling so this method
     * will be reasonably hot in normal use.
     */

    struct journal_context *new_ctx;

    if ((new_ctx = malloc(sizeof(struct journal_context))) == NULL) {
        debug("Failed to malloc new journal context");

        return NULL;
    }

    /* Always good to clear the memory allocated. */
    memset(new_ctx, 0, sizeof(struct journal_context));

    /* Set up some defaults. */
    new_ctx->transaction = NULL;
    new_ctx->commit = NULL;
    new_ctx->updates = new_queue();
    new_ctx->locked = new_queue();

    /* Each context has a fixed size. This isn't entirely necessary
     * and could be implemented in a different way, but this makes
     * determining when to commit and allocate a new context easy. */
    new_ctx->buffer_size = JOURNAL_DEFAULT_CONTEXT_SIZE;
    new_ctx->total_used = 0;

    /* Used is set to 3 by default as we reserve space in the context
     * for the transaction, update meta and commit blocks which surround
     * the actual update blocks (of which we can have JOURNAL_DEFAULT_CONTEXT_SIZE - 3)*/
    new_ctx->used = 3;
    new_ctx->dev = journal->dev->dev;

    return new_ctx;
}

journal_entry_t *journal_register_intent(struct j_handle *jh, dev_t dev, block_t block_number, int flag) {
    /**
     * Registers the intent to modify a block to the journal so that
     * it can add it to the journal if it is modified.
     *
     * This method essentially wraps the get_block function of the
     * cache but adds the block returned to a collection of locked
     * blocks, that will only be unlocked once the context is
     * committed (or the intent is deregistered).
     */

    struct buf *bp;
    journal_entry_t *entry;

    /* Must have a valid handle. */
    if (jh == NULL) {
        return NULL;
    }

    if ((entry = malloc(sizeof(journal_entry_t))) == NULL) {
        debug("Failed to malloc journal entry");

        return NULL;
    }

    DEBUG(("[JFS] Registering intent to modify block #%lu\n", (unsigned long) block_number));

    /* Retrieve the block from cache as usual. */
    bp = get_block(dev, block_number, flag);

    /* Wrap this block in a journal entry so it can be kept
     * in memory for the duration of this context. */
    entry->entry_id = JOURNAL_UPDATE_MAGIC;

    /* Keep the original block number. */
    entry->block_number = block_number;

    /* TODO: Is this even needed if it's not written to disk? */
    entry->checksum = 0;
    entry->sequence_n = jh->ctx->transaction->sequence_n;

    /* Just maintain a pointer to struct buf - no copying yet.
     * Copying will occur when an actual modification is made
     * (i.e. copy-on-write). */
    entry->data = bp;

    /* Add to list of blocks locked in cache */
    enqueue_entry(jh->ctx->locked, entry);

    return entry;
}

int verify_copy(journal_entry_t *update, journal_entry_t *entry) {
    /* Sometimes it's nice to do extra sanity checking.
     * I was having problems with making a copy of a block
     * when journaling, so I added this to verify that
     * the copy actually happened and didn't corrupt anything. */
    int i;

    /* Addresses of struct buf copy and original struct buf pointer
     * should be different. */
    if (&(update->copy) == entry->data) return -1;

    /* Compare each chunk */
    for (i = 0; i < MAX_BLOCK_SIZE; i++) {
        if (update->copy.b_data[i] != entry->data->b_data[i]) return i;
    }

    return 0;
}

journal_entry_t *journal_new_block(struct j_handle *jh, struct inode *rip, off_t position) {
    /**
     * Because sometimes a new block is needed, if this new block needs
     * to be journaled then some way of mediating that needs to be available.
     *
     * This method will allocate a new block number (which is essentially
     * the same as creating the block, but without the actual pointer
     * being returned) and then registers that block in the journal.
     */

    block_t b;
    struct buf *bp;
    journal_entry_t *entry;

    if (jh == NULL) {
        return NULL;
    }

    debug("Allocating new block and registering in journal");

    /* Get a new block number (half the operation of a normal new_block call */
    b = new_block_number(rip, position);

    /* Instead of going directly through the block cache, we must register
     * this new block to be journaled */
    entry = journal_register_intent(h, rip->i_dev, b, NO_READ);

    return entry;
}

int journal_update_block(struct j_handle *jh, journal_entry_t *entry) {
    /**
     * Registers an update to a block that was acquired through the journal.
     *
     * This is where real work happens. Before this method is called a block
     * should have been obtained by registering intent with the journal.
     * When a modification is made, it needs to be registered in the journal
     * and this method performs that by making a copy of the block that was
     * modified.
     *
     * If an update for the block is already in the current context,
     * then an additional entry won't be created, the modified data
     * will simply be copied over (an optimisation to reduce the size
     * of the journal and prevent it from being unnecessarily full).
     */

    journal_entry_t *update;
    struct node *curr;
    int r;

    /* Have we got a valid handle? */
    if (jh == NULL || entry == NULL || entry->data == NULL) {
        return -1;
    }

    /* Search for this update in the updated block list. */
    TAILQ_ENTRY_ITERATOR(curr, jh->ctx->updates) {
        if (curr->q_entry->block_number == entry->block_number) {
            /* Already have an update entry for this block */
            debug("Already have registered update for this block");

            /* Copy the new data to the existing update. */
            curr->q_entry->copy = *(entry->data);

            /* Put this block back (as we already have a lock on it). */
            put_block(entry->data, MAP_BLOCK);

            /* Because items are only added at the tail, we know if
             * we're updating a block we would have just added it to
             * the locked block list (this should be true). */
            remove_tail(jh->ctx->locked);

            /* Even though no more space was used in the journal,
             * it is still classed as space used by the handle,
             * meaning that a context will fill up if updates
             * exist and are copied, but the journal won't use
             * any extra space.
             *
             * This is done to prevent a context sitting around
             * in memory for a long time as we want to keep them
             * regularly rotating to avoid journaled data sitting
             * in memory for a longer time (and becoming more
             * likely to be lost). */

            jh->available--;
            jh->used++;

            return 0;
        }
    }

    /* Didn't find an existing update for this block so it needs to
     * be added to the update list and the data copied. */

    debug("Adding to update list");

    if ((update = malloc(sizeof(journal_entry_t))) == NULL) {
        debug("Failed to malloc new journal entry");

        return -1;
    }

    /* Mimic the entry - the update block will actually
     * be written to disk, but only the data field of it. */
    update->entry_id = entry->entry_id;
    update->block_number = entry->block_number;
    update->checksum = entry->checksum;
    update->sequence_n = entry->sequence_n;
    update->data = entry->data;

    /* Make a copy of the entire struct */
    update->copy = *(entry->data);

    /* Sanity check because I have trust issues with MINIX. */
    if ((r = verify_copy(update, entry)) != 0) {
        printf("Mismatch at position: %d\n", r);

        panic(__FILE__, "Copy of data block does not match original", NO_NUM);

        return -1;
    }

    /* Add to list of updated blocks */
    enqueue_entry(jh->ctx->updates, update);

    /* Claim the space used by this update. */
    jh->available--;
    jh->used++;

    return 0;
}

int journal_deregister_intent(struct j_handle *jh, journal_entry_t *entry) {
    /* Essentially just removes a locked block from the locked list
     * if it wasn't modified by a transaction. */


    if (jh == NULL) {
        return -1;
    }

    DEBUG(("[JFS] Deregistering intent to modify block #%lu\n", (unsigned long) entry->block_number));

    remove_tail(jh->ctx->locked);

    put_block(entry->data, MAP_BLOCK);
}

journal_transaction_t *build_transaction() {
    /* Builds a transaction which is always at the start
     * of a new transaction context. */

    journal_transaction_t *transaction;

    if (journal->header == NULL) {
        error("Cannot start new transaction without the loading journal first",
              JOURNAL_ERROR_NO_LOADED_JOURNAL);

        return NULL;
    }

    if ((transaction = malloc(sizeof(journal_transaction_t))) == NULL) {
        /* Malloc failed */
        debug("Failed to malloc new transaction");

        return NULL;
    }

    memset(transaction, 0, sizeof(journal_transaction_t));

    journal->header->first_sequence_n++;

    DEBUG(("Building transaction: seq = %d\n", journal->header->first_sequence_n));

    transaction->transaction_id = JOURNAL_TRANSACTION_MAGIC;
    transaction->sequence_n = journal->header->first_sequence_n;
    transaction->checksum = 0;

    return transaction;
}
