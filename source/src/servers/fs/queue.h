/* We need a queue which can hold either a struct journal_entry
 * (for locked blocks and update blocks) OR a struct journal_context
 * as these are the data structures we need to maintain lists of. */

typedef union node_data {
    /* A node can either have a journal context
     * or a journal entry as its data. */

    struct journal_context *context;

    struct journal_entry *entry;

} queue_data;

struct node {

    /* Union type for the data of a node */
    queue_data data;
    
    /* We maintain a doubly-linked list to 
     * make some operations more simple. */
    struct node *prev;
    struct node *next;

};

struct queue {

    /* This is a queue where we remove from the head
     * and add at the tail */
    struct node *head;
    struct node *tail;

    /* Because why not keep a track of this. */
    int size;

};

/* Allows for us to get at the different union data types
 * more easily. This idea is borrowed from struct buf. */
#define q_ctx data.context
#define q_entry data.entry

/* Make life easier. Credit goes to sys/queue.h which
 * unfortunately isn't in MINIX.
 * (http://man7.org/linux/man-pages/man3/queue.3.html) */
#define TAILQ_ENTRY_ITERATOR(v, q)        \
        for ((v) = (q->tail);             \
             (v) != NULL;                 \
             (v) = ((v)->prev))

#define TAILQ_REVERSE_ENTRY_ITERATOR(v, q)        \
        for ((v) = (q->head);                     \
             (v) != NULL;                         \
             (v) =((v)->next))


/* Prototypes */

/* Initialise a new queue */
struct queue *new_queue(void);

/* Add to queue */
int enqueue_context(struct queue *q, struct journal_context *ctx);
int enqueue_entry(struct queue *q, struct journal_entry *entry);

/* Remove from queue at end (not generally a queue
 * operation but we need it in this case). */
int remove_tail(struct queue *q);

/* Release all allocated queue entries. Does not release the
 * queue itself which must be done by the caller. */
int free_queue(struct queue *q);
