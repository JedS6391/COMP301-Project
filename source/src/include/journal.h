#include <sys/types.h>

/* An ID for in the Journal header on disk.
 * Basically used to make sure the journal header is valid,
 * and if the journaling was ever extended, it could serve
 * as a version number.
 */
#define JOURNAL_ID 0x2222

/* All errors and debug output */
#define JOURNAL_VERBOSE 4

/* Error and debug output */
#define JOURNAL_ERROR 3

/* Only debug output */
#define JOURNAL_DEBUG 2

/* No debug or error output */
#define JOURNAL_QUIET 1

/* How much logging output is required. */
#define JOURNAL_LEVEL JOURNAL_QUIET

/* Macro version of debug printf, to handle variadic arguments. */
#define DEBUG(s) if (JOURNAL_LEVEL > JOURNAL_QUIET) printf s

/* Error numbers */
#define JOURNAL_ERROR_NO_LOADED_JOURNAL 1
#define JOURNAL_TRANSACTION_CONTEXT_ALREADY_EXISTS 2
#define JOURNAL_INVALID_HEADER 3
#define JOURNAL_INVALID_INODE 4
#define JOURNAL_INVALID_DEVICE 5
#define JOURNAL_ERROR_INIT 6
#define JOURNAL_ERROR_NO_TRANSACTION_CONTEXT 7
#define JOURNAL_NOT_ENOUGH_SPACE 8
#define JOURNAL_MALLOC_FAIL 9
#define JOURNAL_HANDLE_EXISTS 10

/* The size a context can grow to until a commit must be forced.
 * TODO: It could make sense to make the a configurable value
 * based on the size of the journal. */
#define JOURNAL_DEFAULT_CONTEXT_SIZE 32


/* Magic numbers for the different types of blocks that are
 * written to the journal. This is done both as verification
 * and so that we can just blindly read blocks, and react according
 * to what the magic number is. */
#define JOURNAL_TRANSACTION_MAGIC 0xc3c8
#define JOURNAL_UPDATE_MAGIC      0x4eb6
#define JOURNAL_COMMIT_MAGIC      0x98f0


typedef u32_t journal_error_no;


/* Forward declaration for the queue types needed */
struct queue;

/* A wrapper around dev_t to give it a name that makes
 * sense while journaling. */
typedef struct journaled_dev {

    dev_t dev;

} journaled_dev_t;

/*
 * The first block of the journal is reserved
 * and used to store the journal block header,
 * which includes some metadata about the journal
 * and some information about what state the
 * journal is currently in.
 *
 * It could be said that is wasteful to reserve an
 * entire block for about 240 bytes (more with padding)
 * but is information required by the journaling code
 * and it needs to go somewhere!
 */
typedef struct journal_block_header {

    /* An ID for the journal. Currently set to a magic number for
     * this version/implementation of JFS.
     * TODO: Add more than 1 magic number spread across the journal
     * header as a means of detecting corruption. */
    u16_t journal_id;

    /* The size of a block in the journal. Should be the same as the
     * global block size in the superblock (i.e. of the device). */
    u32_t block_size;

    /* Total blocks used by the journal (i.e. the journal size). */
    u32_t total_blocks;

    /* A reference to the very first block used by the journal.
     * We keep this reference so that when the journal is cycled,
     * the head marker can simply be set to first. */
    block_t first;

    /* An index of the first free block in the journal. This
     * index is moved through the journal as contexts are
     * committed. */
    block_t head;

    /* An index to the end of the last checkpointed context.
     * Together with the head index, this creates a region
     * of free space and a region of used space in the journal. */
    block_t tail;

    /* The last block in the journal. Basically used to detect
     * when we go off the end of the journal so we can cycle back.
     * Should be equal to total_blocks. */
    block_t last;

    /* The sequence number of the most recent commit. This should
     * be the sequence number for the context that the tail index
     * marks, as that is always the most recently made commit. */
    u32_t first_sequence_n;

} journal_block_header_t;


#ifndef MKJFS_INCLUDE
/* Block types */

/* A transaction block - marks the start
 * of a context. Doesn't really contain much
 * information but could be extended to contain
 * more state about the context it marks. */
typedef struct journal_transaction {

    /* Magic number to identify this block type. */
    u32_t transaction_id;

    /* Checksum to verify that this block is valid
     * when loaded back. */
    u32_t checksum;

    /* Basically an incrementing counter that identifies
     * the transaction.
     * TODO: Make sure this doesn't overflow. */
    u32_t sequence_n;

} journal_transaction_t;

/* Used to wrap an actual data block that will go into the
 * journal on commit. The structure itself isn't written to
 * disk but it is useful to keep extra information with
 * the block when in memory. */
typedef struct journal_entry {

    /* Currently just set to the same magic number as
     * for an update meta block.
     * TODO: Remove this? */
    u32_t entry_id;

    /* The original block number of the block that this
     * entry is for. Keep this so that when it is journaled
     * we can copy it back to its original location in the
     * case of a journal replay. */
    block_t block_number;

    /* A pointer to a struct buf returned from get_block. */
    struct buf *data;

    /* Updated blocks work in a copy-on-write kind of way.
     * When a block update is registered, it only maintains
     * a pointer to the buffer. When an actual update is made
     * (i.e. journal_update_block is called), only then does
     * a actual copy of the block get allocated to this field.
     * The reason it is a struct, and not a pointer is because
     * MINIX was complaining and doing strange things if I had
     * a pointer to a copy of the block, but allocating the space
     * upfront seemed to stop it from complaining. */
    struct buf copy;

    /* Similar to that of a transaction block. */
    u32_t checksum;

    u32_t sequence_n;

} journal_entry_t;

/* A journal_update_meta is the portion of a
 * journal_entry written to disk as metadata
 * for the actual update block. All it really
 * keeps a hold of is the number of update blocks
 * that are in the context and a list of the original
 * block numbers belonging to each update block. */
typedef struct journal_update_meta {

    /* A magic number to identify this block type. */
    u32_t update_meta_id;

    /* The number of update blocks that follow this
     * metadata block (and also that belong to the
     * context that this update meta block belongs to). */
    u32_t number_updates;

    /* The block number where this block will go. We need
     * a field for this because the update blocks are written
     * to disk before the update meta block, but the meta block
     * needs to logically be in a block preceeding the first
     * update block. */
    block_t block_number;

    u32_t checksum;

    /* List of original block numbers for the updates
     * of a transaction. I tried allocating this dynamically,
     * but ran into problems when writing to disk. Basically
     * what I tried was declaring a pointer, and when mallocing,
     * requesting space for the struct and extra space for
     * an array of block numbers starting at the address of
     * the pointer. E.g.
     *
     * Assuming block_run is defined as block_t *block_run;
     *
     *    journal_entry_t *entry;
     *    entry = malloc(sizeof(journal_entry_t) + number_updates * sizeof(block_t));
     *    entry->block_run = &(entry->block_run) + 1;
     *    entry->block_run[0] = (block_t) 1;
     *    entry->block_run[1] = (block_t) 2;
     *    entry->block_run[number_updates - 1] = (block_t) 3;
     *       ...
     *
     * To workaround this, I statically allocated it and in doing so
     * made the assumption that a context will have no more than
     * (JOURNAL_DEFAULT_CONTEXT_SIZE - 3) update blocks, which
     * should be always true (I hope!).
     */
    block_t block_run[JOURNAL_DEFAULT_CONTEXT_SIZE - 3];

    u32_t checksums[JOURNAL_DEFAULT_CONTEXT_SIZE - 3];

} journal_update_meta_t;

/* A commit block - marks the end of a context. */
typedef struct journal_commit {

    /* A unique magic number for this block type. */
    u32_t commit_id;

    /* As for a transaction block... */
    u32_t checksum;

    u32_t sequence_n;

} journal_commit_t;


/* A journal context represents a compounded set of transactions
 * that are grouped together and written to the journal together.
 * A context is really just a transaction block, followed by an
 * update meta block, followed by some number of updates, followed
 * by a commit block.
 */
struct journal_context {

    /* The block which starts this context of transactions.
     * Basically a "start of sequence" marker. */
    journal_transaction_t *transaction;

    /* The block which ends the context of transactions.
     * An "end of sequence" marker. */
    journal_commit_t *commit;

    /* The updates to the file system (the journal entries).
     * Each update will have an associated block number. These
     * go in a queue so that the most recent updates are at the
     * tail of the queue (i.e. FIFO) so that the first updates
     * written to the journal in memory are also the first to be
     * flushed to the journal on disk. */
    struct queue *updates;

    /* A journal entry is locked until it is checkpointed. This is
     * essentially just a list but having a queue means we can
     * unlock blocks in the same order they were locked. */
    struct queue *locked;

    /* The total number of blocks allocated to this transaction.
     * This includes the transaction start, update meta, and
     * commit blocks. */
    int buffer_size;

    /* How much of the buffer is used up */
    int used;

    /* The total number of blocks in the journal actually written
     * to when flusing this context (so the tail pointer can be updated
     * when a transaction is checkpointed) */
    block_t total_used;

    /* The device this context belongs to. Basically just for sanity
     * checking that we're not trying to journal on a non-JFS. */
    dev_t dev;
};


/* A handle must be obtained to perform any operations
 * on the journal. It basically allows for a caller to
 * request journaling and if the request is granted, all
 * further journaling calls must used the handle so the
 * requests can be verified. */
struct j_handle {

    /* The context this handle is for */
    struct journal_context *ctx;

    /* How many updates were made through this handle.
     * Will be set to 0 by default in all cases. */
    int used;

    /* How many updates can be made through this handle.
     * i.e. when used >= available, the handle is no longer
     * valid and a new handle should be obtained by starting
     * a new transaction. */
    int available;

    /* This must match a journaled device */
    dev_t jdev;

};

/* This structure is only used in memory while journaling
 * and essentially just manages all the state associated
 * with journaling. */
typedef struct journal {

    /* Once a JFS is loaded, we keep a pointer to
     * the journal header so it can be re-written
     * to disk when necessary (i.e. when it is updated). */
    journal_block_header_t *header;

    /* This will always be the current context that is
     * being used to satisfy transaction requests. */
    struct journal_context *context;

    /* Contexts which have been committed, but have not
     * yet been checkpointed. Need to keep them in memory
     * until they are finally flushed to disk. A queue
     * is maintained so that the contexts that were committed
     * first will be written to the journal first. */
    struct queue *committed;

    /* What device is being journaled - for sanity checking. */
    journaled_dev_t *dev;

    /* How many free blocks there are in the journal at
     * any point in time. On a fresh system this will
     * be total_blocks - head. */
    block_t free;

} journal_t;
#endif
