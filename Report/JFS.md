% COMP301 Project
% JFS — Journaling File System in Minix
% Jed Simson 1237893

\newpage

**Warning to the reader:** *This report is relatively long as it discusses not only the goals, steps, and testing that were a part of the project, but also contains a very in-depth discussion of the implementation. As the system is quite complex in its nature, this means the implementation discussion is long - however it was helpful for me to write about the implementation in detail as I worked on it, and has the added benefit of being interesting to anyone who wishes to read it.*

\newpage

# Goal

From the project list:

> *Some modern file systems (e.g. Linux’s ext3) use a journal to implement atomic updates to the file system. This means that, if a crash occurs, a change to the file system (e.g. deleting a file) will be either totally done (removing the file name from the directory, deallocating the i-node and data blocks), or not done at all. A journal is implemented by writing blocks to be changed to a journal rather than to the main part of the disk. After the journal is complete, a flag is written to the journal and the blocks from the journal are written to the main part of the file system. If a crash occurs before the flag is written, the update is lost but the file system will still be consistent. If a crash occurs after the flag is written but before all the blocks have been copied into the main part of the file system, the system will recopy these blocks into the main part of the file system on reboot.*

My goal was to take this idea and to research existing implementations of Journaling File Systems (which I will refer to as a JFS from here) to determine what approaches are available and which would be best to implement. 

What I wanted to see was that I would be able to work on a device with a JFS and if the MINIX machine crashed (i.e. by powering down unexpectedly), I could replay the journal to copy any updates that were not written to the file system, but were written to the journal.

As part of this, I also wanted to have a program external to MINIX that allowed viewing a JFS, simulating replay and traversing the journal on that disk.

Before going further, I need to clarify a common confusion about what journaling means: it generally **does not** make any guarantees about the consistency of data, **only the consistenct of file system data structures**.

The following is what was successfully implemented:

- Atomic updates for file system operations that modify blocks in some way.
- A journal on the device which maintains collections of updated blocks involved in one or more transactions.
- A transaction context, to facilitate compound transactions by buffering multiple transactions into one group.
- Journal commit, which involves writing a context to the journal.
- Journal checkpoint, which involves completing a committed context.
- Journal replay provides a way to copy updates that were written to the journal but did not make it into the file system through means of a checkpoint.
- External disk verification and journal replay simulation tool.

\newpage 

# Process 

**NB**: *This section gives a brief overview of the timeline and how the system was designed and built. For a more robust timeline, the git history is the best resource. For futher details about the implementation, see the [**About**](#about) section which discusses it extensively.*

As this project required the addition of a large amount of complexity to the existing MINIX file system, before I started writing any code I did a large amount of research about existing implementations and literature about Journaling File Systems so that I could: 

1. Gain a better understanding and appreciation for how a Journaling File System really works (to give clarity to some of the *magic* behind it).
2. Prevent myself from jumping in and immediately writing code that wasn't informed by any decisions.

This approach worked well for me in that once I had conceptualised the structure of the system and how journaling would work/interact with the exist file system, I was able to lay down a large amount of code that was a base for everything else (i.e. data structures, magic numbers, etc).

The core data structures, definitions, and types used by the journaling system are found in `/usr/include/journal.h`. This file went through many changes as data structures were adapted and changed but a lot of it once written stayed the way it was.

Once I had an image in my head of how the system would work, I ordered my process of implementation as follows:

## Adapting `mkfs`

I started my implementation with a basic step first - altering the `mkfs` program to create a new file system (my journaled file system). At first basically all I did was change the magic number in the superblock to be a number of my choosing to differentiate my file system from previous MINIX file systems. This magic number is defined in `/usr/src/servers/fs/const.h` as `SUPER_MAGIC` (`0x137F`) and sets a journaled file system apart from a normal MINIX file system and allows the system to identify this upon mount. I continued to add to `mkfs` to do the following:

- Allocate an i-node for the journal (line 426: `/usr/src/commands/simple/mkjfs.c`)
- Build a journal header to go in the first data zone of the i-node allocated for the journal (line 460-475`: `/usr/src/commands/simple/mkjfs.c`)
- Create empty zones allocated to the journal i-node up to some maximum size (i.e. the size of the journal) (line 478-482: `/usr/src/commands/simple/mkjfs.c`)
- Add the journal i-nodes number to the superblock (line 487-488: `/usr/src/commands/simple/mkjfs.c`)

From here, I made changes to `/usr/src/servers/fs/mount.c` to print out a nice message when a journaling file system (i.e. a file system with the magic number I chose) is being mounted, and print out some information about the header. This was the basis of the `init_journal()` method found in `/usr/src/servers/fs/journal_init.c`.

The nice thing about this was that to test that it was working as intended, all I was required to do was read the output given by any output I had added and I could quickly verify that everything was working as expected (ignoring the fact it wasn't doing a whole lot).

## Simulated Journaling

Before any real journaling took place, I created a simulation of how the process would work in a simple C program. This was never committed to the repository, but the logic in it was identical to that found in `journal_transaction.c`, in that it would allocate a buffering zone, allow additions to that buffer for some number of additions, and once that bound is met, it would allocate a new buffer (or in this case just clear the existing one). This is where I started with creating the context buffering and transaction components of the journaling.

This simulation ultimately became what is the `new_transaction` method on line 35 of `/usr/src/servers/fs/journal_transaction.c`. This file went through many changes as the needs and design of the system changed, but the git history is a better resource for this than having me describe every little change.

## Transactions and Journaling in-memory

Once I had a rough implementation of journaling, and the process of allocating and deallocating contexts was working in the way I intended for it to in the final version, I begun to refine the process and make sure that in-memory journaling was working completely before I started writing data to the disk.

I figured that this was a good approach as it meant that if anything went wrong, it would not corrupt my file system and there would be less state for me to manage in my head (which is when programs start to get complex!).

The `struct journal_context` type is the core component of in-memory journaling and is defined on line 259 of `/usr/include/journal.h` as follows:

\newpage 

```c
struct journal_context {

    /* The block which starts this context of transactions.
     * Basically a "start of sequence" marker. */
    journal_transaction_t *transaction;

    /* The block which ends the context of transactions.
     * An "end of sequence" marker. */
    journal_commit_t *commit;

    /* The updates to the file system (the journal entries).
     * Each update will have an associated block number. These
     * go in a queue so that the most recent updates are at the
     * tail of the queue (i.e. FIFO) so that the first updates
     * written to the journal in memory are also the first to be
     * flushed to the journal on disk. */
    struct queue *updates;

    /* A journal entry is locked until it is checkpointed. This is
     * essentially just a list but having a queue means we can
     * unlock blocks in the same order they were locked. */
    struct queue *locked;

    /* The total number of blocks allocated to this transaction.
     * This includes the transaction start, update meta, and
     * commit blocks. */
    int buffer_size;

    /* How much of the buffer is used up */
    int used;

    /* The total number of blocks in the journal actually written
     * to when flusing this context (so the tail pointer can be updated
     * when a transaction is checkpointed) */
    block_t total_used;

    /* The device this context belongs to. Basically just for sanity
     * checking that we're not trying to journal on a non-JFS. */
    dev_t dev;
};
```

\newpage

A context manages the state for a set of compound transactions and was introduced in commit `0a1b4e3e` and is the way the transactions are managed in the final version too.

To test journaling in memory, I first limited myself to testing only one system call - the open system call, specifically when creating a file. I did this so that I could test journaling works properly for one system call, and then adding others would simply be a matter of setting up journaling in the correct locations in the original code to handle the syscall.

## Commits and Checkpoints

Once in-memory journaling was completed and verified to be working as intended, I was able to proceed with actually writing updates to the journal on disk. Reaching this point was not straightforward as to do in-memory journaling, I had to first run into the most painful bug that I encountered while working on the project: stack overflow (or heap overflow, whichever was happening first). 

Because the journaling requires allocating a large amount of memory as transactions complete (especially since at this stage I wasn't flushing anything to disk), it was causing the heap to grow and grow as memory was allocated, approaching closer and closer to the stack until inevitably they would overflow into each other, corrupting the entire memory space of the file system. This would cause different behaviour depending on where the overflow occured but I experienced errors such as messages from the FS getting corrupted, struct fields changing into seemingly random values, and the system just completely halting. 

In the end, I got past this by increasing the size of the stack allocated to the file system when it is installed on line 27 of `/usr/src/servers/fs/Makefile`: 

```makefile
...
install -S 512kw $@
...
```

With this sorted, I could begin flushing data to disk when it is no longer needed in memory. Operations relating to this are found in `/usr/src/servers/fs/journal_commit.c` and all follow much the same format to write data to disk. I chose to use the `dev_io` method to write data directly to the device rather than go through the cache as I could guarantee that the data would be written directly. I did experiment with going through the cache and setting the flags appropriately to flush it to disk immediately, but this meant I had to make all my own blocks into a `struct buf` type and this seemed too complex when I could just go straight to the disk provided I could calculate the offset/position. An example of this method in action can be seen on line 290 of `journal_commit.c` and goes something like:

\newpage

```c
/* Requests a write operation on the device given from the FS. The data
 * to be written is pointed to by `buf` of which `journal->header->block_size`
 * bytes will be written to `pos`. */
dev_io(DEV_WRITE, journal->dev->dev, FS_PROC_NR, buf, 
       pos, journal->header->block_size, 0);
```

When implementing checkpointing, I first added it such that each time a commit occured a checkpoint would also happen. This wasn't ideal as it meant that a lot of file system writes were happening at the same time. In a further version (commit 9f2498cc) I added conditional checkpointing so the operation would only happen occassionally depending on how many contexts had been committed.

## Replaying the Journal

Adding journal replay was really the last step to be done, and the most important part of the journaling. Without it all you end up with is a slower file system that does more writes and uses more memory - not exactly ideal. B

Before journal replay could be added however, it was important that I verified that the journal was actually being written as I expected. This is where my disk verification tool came in and allows for viewing the journal externally from MINIX to verify that its contents are expected.

This process of verification is essentially one of the stages I consider as testing, and I spent a large amount of time changing parts of the code as viewing the journal allowed for me to get a better idea of how the journal was being utilised, etc. The addition of the disk verication tool benefited me greatly and is an essential testing tool for building a journaling file system as far as I am concerned.

Once it was known that the journal was correct and being written to properly, journal replay could be implemented. The way that I had structured previous code made implementing recovery relatively simple (which it should be because it is supposed to be the reason journaling is so great).

Like with many of the previous steps, before I started writing code for the operation in MINIX I created a simulation version to get an idea of how things should work. I added this as part of the disk verification tool and can be found in the final version by toggling the flags appropriately.

This idea of simulation is a second stage that I consider as testing that I found extremely useful and important. It allowed for me to work the implementation out in an isolated way before blindly adding code to the file system, which is always a bad idea as it needs to be done thoughtfully and carefully. 

## Further Journaling 

As stated previously in the [**Transactions and Journaling in-memory**](#transactions-and-journaling-in-memory) section, I limited the journaling to only one system call at first as a way to bound the scope of the system so I could verify everything was working before adding more complexity. 

It turned out that this was a great idea as it meant that when I added more journaled system calls one at a time that they would uncover bugs that may not have appeared if I had added journaling of all system calls from the start.

The system calls I have verified to be journaled correctly are:

- `open()` with the `O_CREAT` bit set (i.e. file creation)
- `unlink()`
- `write()`

Note that really the metadata modified by these calls is what is actually journalled so it is simple to add journaling for further system calls as all that need be done is add the appropriate journal hooks.

## Final Additions

After journal replay was completed and verified to be working correctly (again using the disk verification tool) I could focus on small improvements that I wanted to add to make the implementation slightly better.

This included but was not limited to:

- Various improvements to the code and fixes for subtle bugs
- Various improvements to the disk verification tool
- Checksum calculations for blocks written to the journal (a feature I wanted to implement from the start)

## Missing

There were a number of additions that I didn't quite complete as they required a large amount of changes that the time didn't allow. 

One limitation of the current implementation is that only one JFS can be mounted and used at a time. This is obviously not great if you wanted to have multiple journaled disks. Existing solutions for journaling solve this problem by keeping a list of global journal objects which are associated with the given device. My approach was to attach journaling state to the superblock while it was loaded in memory, and when journaling the correct superblock could be fetched and the journal state for that device loaded.

Another limitation is that journal contexts are artifically limited to a fixed size, which isn't ideal and could lead to the situation where a really large transaction spans across multiple contexts. This is fine in some cases in that all the information is still there, but if some of the updates are not checkpointed then the file system could become inconsistent. A solution would be to set the size of the context dynamically and commit the transaction when the transaction is ended. The way I've implemented is slightly more efficient as it tries to group as many transactions together as it can but it is slightly less logical in its sequencing. This is a trade-off I made and could easily be adapted in the future.

Despite this trade-off, this in a way means that my system performs journaling but also guarantees that it will move from one known good state to another without loss of consistency in between because either the previous known good state will be the state that is kept after a failure, or the system will move to another known good state and the system will be consistent. There is no case where the file system would be in an in-between state.

\newpage

# Usage

The tool `mkfs` has been extended in order to allow creation of a JFS, and this version is available as the command `mkjfs` (installed to `/usr/bin/mkjfs` by default).

**Note:** If for some reason `mkjfs` is not available after a fresh install, it can be installed by navigating to `/usr/src/commands/simple` and executing `make mkjfs` followed by `make /usr/bin/mkjfs`.

## Creating a JFS

Assuming `/dev/c0d1` has a disk attached (e.g. QEMU was started with -hda & -hdb), `mkjfs /dev/c0d1` will create a JFS on the device named `c0d1`.

## Mounting a JFS

Mounting a JFS is no different than mounting an ordinary MINIX file system and can be acheived using the `mount` program:

```bash
# Assuming /dev/c0d1 is a JFS as created above...
mkdir -p /mnt/data
mount /dev/c0d1 /mnt/data
```

Once a JFS device is mounted, any operations performed on that device (e.g. file creations, removals, etc.) will be submitted to the journal in the background and the user can use the drive without any additional complexity - that is, the process is completely transparent.

## Using a JFS

In general operation, the journaling system should perform all of its routines completely in the background. This is a problem, as it means that if the system is working then the user should really have no idea that it is doing so.

That being said, in the journaling code there is a large amount of logging and the amount of logging is configurable. Generally, if debug output is enabled then the number of messages are fairly low. If verbose output is enabled however, there is a large amount of messages given as devices that aren't journaled may call journal routines (and be denied).

\newpage

## Verification of a JFS

To view the contents of the journal on a JFS device, I have included the `verify_disk` program, which loads a QEMU disk image file and reads it as though it a JFS disk. It verifies the superblock and journal header are valid, and then proceeds to traverse the journal until the first empty block in the journal is met. This is useful both to verify that the journal contains what you expect and to get an idea of how a journal is actually represented on disk. The `verify_disk` is not meant to be used in MINIX, and probably wouldn't compile inside MINIX. 

An example output from `verify_disk` is given below:

```
Found transaction block (0xc3c8) at: 197
Found update meta block (0x4eb6) at: 198
Found commit block (0x98f0) at: 200
Found transaction block (0xc3c8) at: 201
Found update meta block (0x4eb6) at: 202
(tail) Found commit block (0x98f0) at: 205
Found transaction block (0xc3c8) at: 206
Found update meta block (0x4eb6) at: 207
(head) Found commit block (0x98f0) at: 209
Reached end of journal
```

The `verify_disk` program also provides a mode to allow *replay simulation*, which will give an output of what would happen if the file system was mounted in its current state, and what would need to be replayed in order to bring the system to a consistent state. An example output is given:

```
================================
Simulating replay of journal
================================
First block not checkpointed: 206
Head: 13
Tail: 9
Replaying journal...
0.00%
25.00%
Copy update 0 to block number 2
50.00%
75.00%

Journal replay complete
```

\newpage

The program can be compiled with gcc, which I have tested on both the lab machines (Ubuntu 14.04.5 LTS) and a virtual machine running Lubuntu 14.04.5.

It can then be invoked from the command line as follows:

```
USAGE: verify_disk [-hrV] <path to disk image>

Options:

-h      Displays the usage message and terminates.
-r      Run the journal replay simulation and report the output.
-V      Verbose mode: prints more information to the output.

```

## See it in action 

*The part you've all been waiting for!*

The problem with a Journaling File System is that if it is working then you really shouldn't know about it, as it should all be happening transparently. This is a problem in demonstrating its abilities, so below I will outline a few ways that the system can be *seen* doing it's job:

**1. Debug Output**

Because journaling operations require a fair amount of work, of course when working on it I needed some way to see what was happening and this was achieved with logging. I created two helper methods `void error(char *msg, journal_error_no err)` and `void debug(char *msg)` which I could use to report messages of varying severity to the output stream. As may be obvious, this would produce a lot of output which may get annoying, so there is a definition (`JOURNAL_LEVEL`) in `journal_transaction.c` that allows toggling of the output level as follows:

+---------------------+----------------------------+
| **Level**           | **Description**            |
+=====================+============================+
| JOURNAL_QUIET       | No debug or error output   |
+---------------------+----------------------------+
| JOURNAL_DEBUG       | Only debug output          |
+---------------------+----------------------------+
| JOURNAL_ERROR       | Error and debug output     |
+---------------------+----------------------------+

There is also `JOURNAL_VERBOSE` which is basically an alias for `JOURNAL_ERROR` but with a slightly more descriptive name.

This will give some indication that the journaling file system is working and what it is doing but it can be quite complex and hard to follow depending on how much output is given.

\newpage

**2. Disk Verification Tool**

The disk verification tool outlined above is also a good way to check that the system is working as intended. It allows for a view of the journal that describes how it sees updates to the system and can be used as a sort of dry-run to see what would happen if a journal replay were to occur for that disk.

**3. Crashing the System**

**Disclaimer:** This section requires some understanding of the system so it is recommended you read the implementation details first, and then it becomes easier to verify that the journaling is working.

Another good way to see the JFS in action is to perform some file system operation (e.g. creating a file) and force quit QEMU while it is in the middle of running (analagous to a unexpected power off). Then view the contents of the journal using the disk verification tool or bring the system back online and remount the device and see if journal replay brings the device back to a state that is consistent.

For sake of completeness I have provided an example of what happens from the journal's point of view for a set of operations on a file and how journal replay works in that case.

**Scenario:** A new, empty file is created, then some data written to it, and then the file is deleted.

*NB: Assume /mnt/data is the mount point for a device with a JFS.*

```bash
# Create empty file
touch /mnt/data/test

# Write some data to the file
echo "This is a journaling test." > /mnt/data/test

# Delete the file (pretty silly example but it demonstrates a point!)
rm /mnt/data/test
```

If you were to list the operations that these commands perform relative to time it would go:

- To create file: Allocate i-node for new file and add filename to directory.
- To write to file: Allocate zone(s) and assign to i-node for the file.
- To delete file (assuming no other links to file): Remove directory entry, deallocate any zones allocated to the files i-node, deallocate the i-node. 

As can be seen there are quite a few blocks that would be modified as a part of these operations, but the journaling file system will only journal blocks that contain metadata (this is further explained in the implementation section) so the operations would be journaled something like:

\newpage

- **File create:** 
    - Modified i-node bitmap
    - New i-node
    - Modified i-node for directory entry addition
- **File write:**
    - Modified zone bitmap
    - Modified i-node
- **File delete:**
    - Modified i-node for directory entry removal
    - Modified zone bitmap
    - Modified i-node bitmap

If the disk verification program is run after a commit to the journal following the above operations, it should read something like:

```
Found valid superblock at offset 1024
Found valid journal header at 196
Block size (bits):     4096
Journal size (blocks): 512
Journal head:          8
Journal tail:          8
Found transaction block (0xc3c8) at: 197
Sequence number: 1
Checksum:        0x4097dcb3
Found update meta block (0x4eb6) at: 198
Number of updates:   4
Checksum:            0x62227756
Update block number:   2
Update block checksum: 0xff000000
Update block number:   4
Update block checksum: 0x46b5f297
Update block number:   195
Update block checksum: 0xa505df1b
Update block number:   3
Update block checksum: 0x6906c909
Found commit block (0x98f0) at: 203
Sequence number: 1
Checksum:        0xabb1e834
```
  

This indicates that there were 4 block updates journaled as part of this context:

- **Block #2** - Modified i-node bitmap:
    
    The file creation will cause a new i-node to be created and this means setting a bit in the i-node bitmap, which is at block 2 (for simplicity, details about this are omitted).

    This same block would be modified again when the file is deleted, and the bit for the i-node in the map is free'd - but as the journaling file system aggregates updates together for efficiency (i.e. if the same block is modified more than once, only the most recent update is actually stored and copied over on replay), the block only appears in the list once.

- **Block #4** - Modified i-node:

    This seems to correspond to the first i-node (as MBR is block 1, i-node bitmap at block 2, and zone bitmap at block 3), which I assume would be the root i-node of the root directory.
  
- **Block #195** - Modified directory block:
    
    Assuming the file system is fresh (i.e. just been created and completely empty), because we are creating a file in the root directory, the root directory block will be modified to have an entry for the new filename. It makes sense that this would be block 195 as the journal i-node has its first data zone at block 196 (from line 2 above), and the first block before that would be the first data zone allocated when the file system is created (i.e. `superblock->s_firstdatazone`) which would be 195 and is reserved for the root directory (as per pg. 552, Operating Systems Design & Implementation).

- **Block #3** - Modified zone bitmap:

    When the file is actually written to, a data zone will be allocated for the files i-node where the data will be stored. This is not journaled, but the allocation of a bit in the zone bitmap is journaled, and this is at block 3 (i.e. directly after the i-node bitmap).

From the analysis above, it can be seen that a lot of verification (and sanity checking) can be performed from reviewing the state of the journal after operations if required.

\newpage 

# Thoughts

I found the experience of working through this project rewarding, frustrating and confusing all at the same time.

That being said, I feel it made me a better programmer as I further learnt C (plus the complexities that come with a lower level language like it), and I gained an appreciation for working on a large, complex code base like that of MINIX. I also gained a greater understanding of file systems as I had to wrangle the FS code, and I feel like choosing to implement journaling allowed me to gain an insight into a really interesting extension to file systems.

To further describe my experience I give a breakdown of some of the highlights and lowlights.

## Highlights

- Adding a change to the code that I thought would break everything and have MINIX compile just fine, leading to me questioning my own sanity.
- Staring at the computer for extended periods of time wondering how on earth the behaviour I was experiencing was even possible (in this case it turns out I was creating a stack overflow and corrupting the entire FS memory space).

## Lowlights

- Realising that all my problems could be solved by changing one line of code.
- Freeing memory I shouldn't have and seeing the whole FS get overwritten with random blocks in front of me.

\newpage

# About

This implementation of a Journaled File System is heavily based upon that of the ext3 File System[^1]and the Be File System (BFS)[^2], both of which have extensive documentation that can be found in the acknowledgments section.

[^1]: https://en.wikipedia.org/wiki/Ext3
[^2]: https://en.wikipedia.org/wiki/Be_File_System

## Implementation

### Disk Layout

The layout of a JFS is nearly identical to that of a normal Minix file system, as far as the Master Boot Record and Super Block are concerned. However, there a few additions to facilitate journaling which are described further in this section.

In this implementation - as with many other journaling implementations - the journal itself is a reserved section of the disk which is used to record updates of changes to the disk so that in the event of a crash, the system can be brought back online quickly, as the journal describes a *path* which can be followed in order to make the file system consistent if it is not already. The section about journaling will further cover what is actually journaled and how this guarantees consistency in the file system.

When a JFS is created (using `mkjfs`), an i-node is allocated and reserved which is used to reference the journal. This i-nodes number is stored in the superblock so that the journal can be loaded quickly when mounting a disk, allowing for further information about the journal and its state to be sought. The superblock also stores the size of the journal, which is set to 512 blocks by default (but is easily configurable e.g. `mkjfs -j 1024 /dev/c0d1`).

### Mounting

When mounting a disk, the mount system call will check if the magic number in the superblock (`s_magic`) matches the JFS superblock magic number (defined in `/usr/src/servers/fs/const.h`). If it does not, then the disk is mounted as a normal Minix disk. If the magic number does match, then the disk contains a JFS and the process of journaling on that device can begin.

The process starts by first attempting to load the journal header structure from the disk. The journal header is stored in the first zone allocated to the journal i-node and has a fairly simple structure as shown on the following page:

\newpage

```c
typedef struct journal_block_header {

    /* An ID for the journal. Currently set to a magic number for
     * this version/implementation of JFS. */
    u16_t journal_id;

    /* The size of a block in the journal. Should be the same as the
     * global block size in the superblock (i.e. of the device). */
    u32_t block_size;

    /* Total blocks used by the journal (i.e. the journal size). */
    u32_t total_blocks;

    /* A reference to the very first block used by the journal */
    block_t first;

    /* An index of the first free block which we are currently
     * on in the journal. */
    block_t head;

    /* An index to the end of the last checkpointed context */
    block_t tail;

    /* The last block in the journal */
    block_t last;

    /* The first sequence of journal entries expected. */
    u32_t first_sequence_n;

} journal_block_header_t;
```

\newpage

This structure is used not only to determine that the JFS is valid (with the aid of magic numbers), but also as a way to store some information about the file system (i.e. block size, number of blocks) and the current state of the journal.

If the journal header is deemed valid then the FS is successfully mounted and can be operated on normally (with journaling happening in the background).

### Journaling

To understand the journaling process, there are a few terms that need to be explained:

Transaction
  ~ A transaction represents some atomic update to the system. When modifying a block, it is thought of as a transaction.

Context
  ~ A context is simply a grouping of transactions for efficiency purposes (i.e to avoid writing to disk a lot).

Update
  ~ An actual change to some disk block that the journal cares about, and as a result is added to the journal.

Commit
  ~ The ending of a context is referred to as a commit, which essentially means that the context is written to the journal. This doesn't mean the context is complete (i.e. able to be discared) yet.

Checkpoint
  ~ A checkpoint brings any currently committed contexts to completion, by freeing any blocks locked by that context and freeing any data structures used by the context. Once a commit is checkpointed, the space in the journal used by that context can be reclaimed.

**Block Types**

- Transaction Block (`0xc3c8`)
- Update Meta Block (`0x4eb6`)
- Commit Block      (`0x98f0`)

*NOTE: The block types are described further below.*

The heart of the journaling operations lies with a data structure I've referred to as a `transaction context`. A context is a compound set of transactions that are grouped together and written to the journal together. It is possible to have a system that creates a transaction for some atomic operation and then writes it immediately to the journal, but it is much better for performance to group the transactions together and write a larger update all at once.

A transaction context is a unit of transactions that has some fixed size. Transactions (i.e. updates to the disk) can be added to a context as long as that context has space for the updates. If this condition is broken, then the context must be committed (i.e. written) to the journal and a new context allocated to handle the next set of transactions. A new context can be allocated provided that there is enough space in the journal (on disk) for a new context.

\newpage

A context has a standard structure, which is basically identical to how it is written to the journal. This is described below:

- A context starts with a Transaction Block which marks the beginning of a set of updates that belong to that context.
- The Transaction Block is followed by an Update Metadata Block, which simply contains information about the update entries in the journal. This is mainly used to keep the original block numbers of the updates, so that if it is necessary to perform recovery, they can be returned to their original locations on disk.
- Next comes some number of Update Blocks, which are simply a copy of the blocks from the main part of disk, after the modifications have been made.
- Finally, comes a Commit Block, which signifies the end of a context and is the last block to be written. If the commit block is written then the context is completely valid in the journal.

![Diagram of a simple transaction context as it would appear on disk after being committed.](images/Transaction Context Block Diagram.png)

Once a context is allocated and ready to accept new transactions, the transactions happen in a two part process:

1. Some code that is going to modify a disk block registers its intent to modify that block to the journal.
2. The code submits an update for the block it registered intent to modify.

These two processes are described further in the following sections, but before that it is worth mentioning the type of journaling used in this implementation.

Generally, there are three different journaling methodologies used which each have various advantages and disadvantages. The way I will descibe them is the way that the ext3 file system refers to them.

1. **Writeback:** Records metadata changes to the journal.
2. **Ordered:** Records metadata changes to the journal, but writes the data changes to the device first.
3. **Journal:** Records both metadata changes and data to the journal.

\newpage

This implementation uses the *Writeback* method, as changes are written to the journal before anything else can happen. This means that there is a possibility that a files metadata may be updated but the files data not be and cause corruption, which *Ordered* journaling helps to prevent (with additional overhead).

I wished to implement *Ordered* journaling as well (and make it configurable) as it provides a safety benefit, but it didn't make it into the final version.

Note that the *Journal* method would be the worst in terms of efficiency as the journal would contain twice as much information, meaning it would fill up quickly, plus a large number of writes could happen for a simple operation (both to the journal and to the main part of the file system).

Now to alleviate the confusion of what is classed as *metadata*, here is a definitive list:

- i-node bitmap blocks
- Zone bitmap blocks
- i-node blocks 
- Directory blocks

While implementing the journal, I went through all the syscalls that modify the file system in some way and tried to determine a common set of data modified and this list is what I came up with as if the data that these blocks describe is consistent, then so to is the file system (as far as the journal is concerned - user data may not be consistent but this is not the purpose of journaling.)

### Registering Intent

An operation needs to register its intent to modify a block before it is able to modify the block itself. A register of intent is really just an adapter for access to the block cache which allows for the journal to lock blocks that are to be modified.

When a register of intent for a block is made, the block requested is fetched from the cache as per a normal `get_block` operation. Usually this would be accompanied with a `put_block` operation once the modification is made, and the block will be flushed to disk when at some stage it is evicted from the cache.

However, in a JFS the block requested is *locked* in cache so that it can not be evicted from the cache until the journal is done with it. This is done so that any data that is modified is not written to the disk until the JFS has written any updates for the block to the journal and committed those updates.

This guarantees the following in the case where a transaction is made (i.e. some blocks are modified):

- If the system crashes anytime before the entries for said blocks are committed to the journal, then the file system will still be consistent as the modified blocks are still locked in cache and so nothing needs to be done.
- If the system crashes after the journal entries are committed but before the modified blocks are fully written to disk, when the disk is mounted again, the journal will recognise that it has a committed but not completed entry, and the modified blocks will be copied from the journal to the disk, bringing the file system to a consistent state.
- If the system crashes after the journal entries are committed and after the modified blocks have been written back to the disk then nothing is done as the journal considers the transactions complete and thus the file system is consistent.

This is one of the core components of the JFS and highlights why it essential that the modified blocks be locked in cache, because otherwise a change may be made to the disk before it is written to the journal, and any guarantees that the journal can usually make are invalid.

### Updating blocks

If a block to be modified has been obtained by registering intent then an update to that block (to be recorded in the journal and written to the original blocks location at some point in time) can be made. This is a simple operation that involves making a copy of the data and keeping a reference to the original block number that the data came from. Both of these pieces of information are submitted to the journal, and are what will be used if recovery is needed.

One optimisation which is made with regards to this process is that if an update for the same block is submitted to the journal multiple times within a context, then only one copy of that update will be kept, as the journal is ordered relative to time.

\newpage

### Commit

When a transaction context becomes full, it must be committed to the journal. The process of committing simply means that any data from the current context is written to the journal. This process is done in three main steps:

1. The Transaction Block is written to the journal.
2. Update Meta Block is created (and a block reserved), Update Blocks are written and once all are done, the Update Meta Block is written to the block reserved for it.
3. The Commit Block is written to the journal.

One implementation detail that has been omitted so far is how the journaling code keeps a track of where it is in the journal on disk, in order to determine the amount of free space in the journal for new contexts, or to facilitate journal replay based on the state of the journal when loaded.

The way this is done is by keeping two markers to mark points in the journal that are required. These are referred to as the `head` and `tail` of the journal. The workings of these markers is relatively simple:

- When a new JFS is created, head and tail both point to the first block that can be used for storing journal contexts.
- Free space is calculated by taking the difference of the total number of blocks in the journal and the start of the journal (i.e. `total_blocks - head`).
- When a context is being committed, each block that is allocated from the journal moves the head marker along by one block and decrements the amount of free space available by one block.
- The journal is circular in nature, so if the head reaches the end of the journal (`total_blocks`), then it is wrapped to the beginning.
- The tail marker is only moved along when a context is checkpointed - that is any blocks belonging to that context are finally written back to their original location on disk, and the context can be completely forgotten (and the space reclaimed in the journal).

I have tested a fair amount that this works as expected, but I feel that there is a chance that there are some subtle problems, especially with handling wrap-arounds. 

### Checkpoint

A checkpoint is similar to a commit but much more definite in nature. A commit refers to the fact that the journal is consistent as all blocks that belong to the transaction have been written to the journal, so that it now describes the file system at that point. A checkpoint is when all the blocks that belong to a context are committed to disk, meaning that the context can be forgotten about and the space reclaimed as the file system is now past that state.

In the current implementation, checkpointing happens when there are a few buffered contexts that have been committed but are waiting for checkpoint. I wanted to implement it so that the blocks belonging to a context had some kind of function pointer and when the blocks were to be evicted from the block cache, they would call that function to disconnect themselves from the context they belong to - but this turned out to be more complex than I anticipated and did not make it into the final version.

### Replay

The head and tail marker not only provide a way to determine the amount of free space left in the journal at a given time, but also inform the journaling system of the state of a journal when it is first mounted. The head marker can be thought of as a reference to the first block after the last committed block - that is the block after the last context that was committed. The tail marker keeps a reference to the last block checkpointed. The difference between the two markers are any commits which have not been checkpointed - that is, they have been written to the journal but the actual updates that belong in that context have not been flushed to their original locations on disk. 

It could be said then that these markers descibe what needs to be added to the file system to bring it a consistent state - and this is exactly what the replay functionality does.

When mounting a device that contains a JFS, the journal header will describe the head and tail markers when that device was last written to. If th head and tail markers are at the same block in the journal, then we can assume that the entire journal is free as the changes in the journal have been both committed and checkpointed (i.e. they are in the journal and on disk).

If the tail marker is not equal to the head marker, it means that any blocks between the tail and head marker need to be copied over to the file system in order to bring it to a consistent state. This is a simple operation that involves:

- Beginning at the block the tail marker references, for each context following: copy any update blocks over to the file system and increment the tail marker along.
- When the tail marker equals the head marker, the operation is done and the state of the file system will reflect the journal's view of the system.

\newpage

![Diagram illustrating the difference between checkpoints and commits.](images/Commited vs Checkpointed.png)

As the diagram shows, contexts that have been checkpointed are those that come before the tail pointer and meaning that they are completely written to the journal and to the main part of the file system. The committed contexts are those after the after the tail marker and up to the head marker and are written to the journal but have not yet been written to the main part of the file system. It is obvious then that the amount of used space in the journal is the difference between the head and the tail markers, and that any updates between the markers need to be copied to bring the system up-to-date in the event of a replay.

\newpage
# Sources/Acknowledgements

I used a lot of reference material in creating this implementation as I did not want to *re-invent the wheel*, as existing implementations have been tried and tested.

I took inspiration from the journaling file system design in Linux (ext2) and in BeOS (BFS) as these were the two file systems that originally made the idea of a journaling file system popular. A list of full sources can be found below, but I would like to make particular mention to *Practical File System Design with the Be File System* written by Dominic Giampaolo (who now works on file systems for Apple).

This book had a lot of useful information about journaling, and in particular went in depth into some of the implementation details which a lot of other texts omitted.

- Dominic Giampaolo. Practical File System Design with the Be File System
- Stephen C. Tweedie. Journaling the Linux ext2fs Filesystem
- http://lxr.free-electrons.com/source/fs/jbd/?v=3.6
- http://lxr.free-electrons.com/source/fs/ext3/?v=3.6
- http://www.ibm.com/developerworks/library/l-journaling-filesystems/
- https://www.usenix.org/legacy/event/usenix05/tech/general/full_papers/prabhakaran/prabhakaran.pdf
- http://www.linux-mag.com/id/1180/
- http://courses.cms.caltech.edu/cs124/lectures/CS124Lec26.pdf
- Checksum implementation from: http://www.hackersdelight.org/hdcodetxt/crc.c.txt

When working on this project, I also came across someone else's work to add a journaling file system to MINIX (http://www.nieklinnenbank.nl/download/jmfs.pdf). I used this as a reference for how to structure some components of my system, but this person's implementation is directly based on the ext2 implementation so I only really gained ideas that I already knew from previously reading about the ext2 file system. Additionally, there is no source with this implementation other than header files (at least as far as I can tell).

\newpage
# API

The journaling functionality of JFS has been merged into the existing Minix 3 File System and provides a few additions to the API which are listed below:

### Global state

**`journal_t *journal`**

The implementation of JFS requires some global state to ensure that any journaling operations are being perfomed on a device that actually supports it. When a device with a JFS on it is mounted, the initialisation routines will create a global journal state variable which references the device with the JFS, the journal header, and any transaction context. If another device with a JFS on it is mounted after one is already mounted, then it will fail as the journal state of the currently journaled device cannot be overwritten.

The limitation that this imposes is that there may only be one device with a JFS being journaled at any given time. This could be fixed by keeping a list (or some other collection) of journal states, but this increases complexity greatly.

**`block_t *journal_block_table`**

When a JFS is mounted, the device is indexed in a way so that all of the block numbers of that device are loaded into a block table, so that any allocations of blocks can be given straight from the table.

**`struct j_handle *h`**

Each time some code wishes to perform an operation that needs to be journaled (i.e. a transaction), it must make a request for a transaction handle, which is kept global, as there should only be one valid handle at any given time, since the system is not multi-threaded.

**`journal_error_no error_n`**

Set appropriately when some error occurs in one of the journaling API calls. It is up to the caller to check for the error number if it expects an error to occur and do the necessary tidy-up. A listing of the errors are given below:

\newpage
+----------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| **Code** |                                                                               **Description**                                                                          |
+==========+========================================================================================================================================================================+
|     1    | When a new transaction or transaction context is requested but a journal has not been loaded (i.e. no device with a JFS has been mounted).                             |
+----------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|     2    | When a new transaction context is requested but there is an existing transaction context which has not used up it's entire buffer space.                               |
+----------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|     3    | A journal header was loaded from a device with a JFS, but the journal header was not valid for some reason (i.e. magic numbers don't match, block size doesn't match). |
+----------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|     4    | The journal i-node was not valid as it's mode, user ID or group ID does not match what it was set to be by `mkjfs`.                                                    |
+----------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|     5    | Occurs when some journaling operation is attempted on a device that does not support journaling (i.e. does not have a JFS).                                            |
+----------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|     6    | When initialisation is attempted and a journal has already been loaded.                                                                                                |
+----------+------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

### Methods

There are four main modules used by the journaling system:

- `journal_init`: Functionality regarding loading, unloading and verifying a journaled volume.
- `journal_transaction`: Responsible for creating contexts, allocating handles and registering updates within a context.
- `journal_commit`: Provides methods to flush a context (i.e. commit) and to checkpoint any outstanding committed contexts.
- `journal_replay`: Allows for replay of a journal if its markers indicate a discrepancy between the state of the journal and the state of the file system.

The following methods are sorted by the module they appear in.

\newpage

#### `journal_init`

___

#### `int init_journal(dev_t dev, ino_t inode_num)`

Initialises the journal by attempting to load the journal header from the device given. The i-node number refers to the journal i-node which is described in the superblock of a device which contains a JFS.

The caller of `init_journal` (which will generally be `do_mount`) should check that the device contains a JFS first by checking that that `s_magic` field in the superblock matches `SUPER_JOURNAL`.

If the journal load is successful then the global journal state will be created and information about the device and journal attached to it, ready for creating transactions.

The integer returned is simply a status indicator, and will be 0 when the initialisation runs successfully, -1 otherwise. If an error occurs, then the

#### `int deinit_journal(dev_t dev)`

The partner of `init_journal` - cleans up the global journal state and will send out anything to the device that needs to be in order for it to be in a consistent state (in terms of the journal).

Takes the device being journaled (which should match whatever device is attached to the global journal state).

The integer returned is a status indicator like most of the journaling API calls, where 0 means success and -1 means an error occured (and as the "contract" of this API governs, the caller will set `error_n` accordingly).

#### `int load_journal_header(dev_t dev, ino_t inode_num)`

Responsible for attempting to load the journal header from the device given into the global context of the FS. The i-node number is the i-node of the journal (which is found in the devices superblock).

There are some sanity checks performed (i.e. the i-node exists, if it exists then it has the correct mode (as set by `mkjfs`)), and confirms that the magic number in the journal header is valid (as a versioning check and also as a simple check for corruption).

If the block containing the journal header is successfully fetched from the disk and all checks are passed then we load it into the global state of the FS and keep a reference to the device that this journal was loaded from. Initially there is no transaction context initialised with the journal, as this will be initialised whenever the first transaction occurs (unless there are some committed changes which were not checkpointed, in which case the recovery mechanisms will create the transaction context from when the journal was last used and perform their associated tasks).

Additionally, this method is responsible for loading the block table which maps blocks in the journal that are able to be used for writing updates to.

If the above process is successful then 0 will be returned, otherwise -1 and `error_n` will indicate what went wrong.

#### `journal_transaction`

---

#### `struct j_handle *new_handle(int available)`

Allocates a new handle that updates to the context can go through. The parameter `available` sets a limit on the number of updates that can be made through the handle returned.

A handle is required for any operations that want to modify the current context and must be destroyed by calling `end_transaction` with the handle to destroy.

#### `struct j_handle *new_transaction(dev_t dev, int space_required)`

The name of this method may be slightly confusing as to what it actually does. `new_transaction` is called wherever a series of instructions knows that it is going to modify some number of blocks on the device given. The `space_required` argument is used to indicate how many blocks will be modified, and thus how much space in a context the transaction will use up. If the current context has enough space for the transaction request then a handle (`j_handle`) will be returned which is used when the caller wishes to perform an operation for the transaction it requested. If there is not enough space in the current context, then a new context will be created ready to accept the transaction request. As a result, `new_transaction` may cause a commit, meaning that a current context may be flushed to disk if its buffer has been exhausted or it does not have enough space for the request.

#### `int end_transaction(struct j_handle *jh)`

`end_transaction` is the counter method to `new_transaction` and is basically responsible for updating the markers for the context buffer and discarding the handle for that transaction.

It is required that every `new_transaction` call must have a corresponding `end_transaction` call, to avoid leaking handles to transactions that are unable to use them (e.g. a non-JFS device that attempts to use the handle to log its updates).

#### `struct journal_context *journal_new_context(void)`

Creates a new, empty context to be used for transactions. This method isn't intended to be used publicly, and is used by `new_transaction` to allocate a new context when the current context becomes full.

#### `journal_entry_t *journal_register_intent(struct j_handle *jh, dev_t dev, block_t block_number, int flag)`

This method can only be used once a valid handle has been obtained (through `new_transaction`) and is responsible for signaling to the journal that the transaction in progress wishes to be granted access to the block with the block number given on the device given, and will be modifying it in some way. This is called a *"register of intent"*, as the code which is performing the transaction to be journaled is registering its intent to modify a block, and that block will go through the journaling code to facilitate the various added complexities that the journal needs to manage rather than coming straight from the block cache.

When an intent is registered, the block requested will be fetched from the block cache as a normal `get_block` operation. The difference however, is that this block is added to a list of *locked* blocks - blocks that haven't been returned to cache which causes them to be locked and unable to be evicted from the block cache - so that it will not be flushed to disk until the transaction for that block is completely finished.

The return is simply a wrapper around a `struct buf *` that allows for it to passed around with some additional metadata and added to queues and other data structures involved in a context.

#### `int journal_deregister_intent(struct j_handle *jh, journal_entry_t *entry)`

If a block is obtained by registering intent and then is not modified, then it needs to be removed from the locked blocks list, as otherwise it will never be flushed to disk as there will always be some reference to it. This method is used to tell the journaling code that it is no longer necessary to lock a certain block in cache.

#### `int journal_update_block(struct j_handle *jh, journal_entry_t *entry)`

Given a valid handle and a journal entry obtained by registering intent to modify a block, `journal_update_block` is the main method that actually modifies the context in some way. Essentially, when the method is called it makes a copy of the data block that the journal entry usually just maintains a pointer to. This copy is completely separate to the pointer copy and is the version that will be written to the journal when the current context is committed. `journal_update_block` takes a snapshot of the data for that disk block at the point that is called.

#### `journal_commit`

---

#### `int journal_commit(void)`

Mainly responsible for creating the commit block and firing off other methods to flush the context. Also updates the queue of committed contexts to include the context currently being committed.

#### `int journal_flush_header(void)`

Because information about the head and tail position in the journal are stored in the journal header, the header needs to be re-written when each context is committed to reflect the state of the journal. This method does that by directly flushing the header block to its position in the journal.

#### `block_t journal_alloc_block_number(void)`

When flushing a context, it is necessary to allocate block numbers that are available to be used within the long. This method simply allocates a block number from the current position of the head marker (cycling when necessary). If there is no free space in the journal then no block will be allocated (as there aren't any to allocate - kind of self-explanatory!)

#### `int journal_flush_context(void)`

This method has three steps (which are each in their own methods):

1. Flush transaction block
2. Flush update metadata block and update blocks
3. Flush commit block

These steps and their corresponding methods will be highlighted further in the following sections.

#### `int journal_flush_transaction_block(void)`

Each context has a transaction block which indicates the start of that context. This is the first block to be written out to the journal. The block doesn't really contain a lot of information other than a sequence number (for that context) and a checksum. It's main purpose is to serve as a *start of context* marker.

This method has a side-effect in that it will free the transaction block from the current context, as it is no longer needed anymore (as it is in the journal), and we want to avoid memory leaks.

#### `int journal_flush_update_blocks(void)`

Writes the update metadata block and any update blocks to the journal. Because the update metadata describes the updates, it is written to the journal after all the updates have been flushed. This is done so that the metadata block can keep a list of the original block numbers for the updates, allowing for replay to place the updates in the locations on disk that they belong.

As the method iterates through the update block queue, it frees the journal entries from the queue, mainly as a way to save some memory because it is assumed that when they are written to the journal we can essentially forget they ever existed. 

The update metadata block is also freed once it has been written to the journal.

#### `int journal_flush_commit_block(void)`

The commit block serves as *end of context* marker, so it is written last (after all the update blocks).

The commit block is also freed from the current context when it is written, to free some memory space up for the next context. 

#### `block_t journal_alloc_block_number(void)`

As described earlier, when mounting a device with a JFS, a table of all the block numbers belonging to the journal is created so that it can be consulted for block numbers.

This method simply returns the block number where the head currently points if there is free space in the journal.

#### `int journal_commit(void)`

Builds a commit block for the current context and adds the current context to a queue of committed contexts (essentially contexts which are awaiting checkpoint).

There are also side-effects in that it begins the flushing of the current context to disk and updates the journal header (on disk and in memory).

#### `int journal_flush_header(void)`

The journal header is modified while it resides in memory, as it keeps track of the head and tail markers. Of course this means that it needs to be written out to disk when committing a context and checkpointing a collection of contexts.

This method simply writes out the journal header to its position on disk. 

#### `int journal_checkpoint(void)`

The checkpointing is really dum in this implementation and could do with being made a bit smarter.

It essentially loops through the queue of committed contexts and performs the following steps:

1. Unlock all blocks that are locked in cache by that context.
2. Update the tail marker to reflect the space freed by checkpointing that context (the amount of free space is also updated).
3. Free any structures belonging to old contexts to prevent memory leaks. Some sanity checking is performed to ensure that all structures relating to a context are both free'd and set to `NULL`.

These steps are performed for every context currently not checkpointed, and once the entire operation is complete, the journal will be completely empty (as far as the journaling code sees it) and the file system will be up-to-date and consistent.

#### `journal_replay`

---

**NB:** Journal replay is implemented in a very simple way and is probably prone to lots of different issues. However, I did not come across any during testing so I am assuming most things work as intended.

#### `int journal_replay(void)`

The journal replay essentially loops over the blocks in between the last checkpointed block and the last committed block and copies any updates that are in between those two points. Those points are the tail and head marker respectively, so it makes the operation relatively simple as all information required can be gained from the journal. 

Journal replay only ever occurs on mount of a device with a JFS, and happens in the background (i.e there isn't a prompt to the user).

\newpage


