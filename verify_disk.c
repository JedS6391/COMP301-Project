#include "jfs.h"

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#define BLOCK_SIZE 4096
#define SUPERBLOCK_OFFSET 1024
#define BAD_BLOCKS_LIMIT 10

#define EMPTY_BLOCK 0x0

/* Prototypes */
FILE *load_image(char *path);
void move_to_block(FILE *f, int block_number);
void *read_block(FILE *f, int block_number);
struct super_block *read_super(FILE *f);
journal_block_header_t *read_journal_header(FILE *f, block_t block_number);
void start(FILE *f, int block_number);
int simulate_replay(FILE *f, int block_number);
void usage(void);
uint32_t crc32(char *data);

int VERBOSE = 0;
struct super_block *super;
journal_block_header_t *header;


#define DEBUG(ARGS...) if (VERBOSE) printf(ARGS)

/* This program allows for verification that a block number on disk
 * contains the values expected.
 *
 * It reads the disk image as a file and seeks to positions accordingly
 * where a "block" in MINIX would be and reads the data from that
 * block appropriately.
 */

int main (int argc, char *argv[]) {
    /* Parses command line arguments and verifies
     * parts of the disk image are valid before starting
     * to read the journal */

    FILE *f;
    char *filename;
    int block_number;
    char c;
    int idx;
    int replay = 0;

    /* Get any options */
    while ((c = getopt (argc, argv, "hrV")) != EOF) {
        switch (c) {
            case 'V':
                VERBOSE = 1;
                break;
            case 'r':
                replay = 1;
                break;
            case 'h':
                usage();
            default:
                usage();
        }
    }

    /* There should be no more than one positional argument following
     * the options. */
    if (argc - optind > 1 || argc - optind <= 0) {
        usage();
    }

    filename = argv[optind];

    /* Load the disk image that was given to operate on. */
    f = load_image(filename);

    /* Read the superblock */
    super = read_super(f);

    /* This won't be quite right but will be close to the start of the journal. */
    block_number = super->s_firstdatazone;

    /* The first block will be the root directory i-node so we can skip it. */
    block_number++;

    /* The second block will be the data zone of the journal i-node.
     * For now, just try to get the magic number from the first 16-bits
     * and validate it is correct. */
    header = read_journal_header(f, block_number);

    /* Move along */
    block_number++;

    /* Start reading the journal */
    start(f, block_number);

    if (replay) {
        simulate_replay(f, block_number);
    }

    /* Done - tidy things up */
    free(f);
    f = NULL;

    exit(EXIT_SUCCESS);
}

void usage(void) {
    /* Prints a usage message and stops the program. */

    printf("USAGE: verify_disk [-hrV] <path to disk image>\n");

    exit(EXIT_FAILURE);
}

void start(FILE *f, int block_number) {
    /* Starting at the block number given, attempts to
     * reads the blocks that make up the journal and
     * decipher some meaning from them. Blocks are identified
     * by a magic number and the end of the journal by the
     * first empty block. */

    /* Stop if we see enough bad blocks */
    int stop = 0;
    int bad_blocks = 0;
    int i;
    int mark = 1;

    journal_transaction_t *transaction;
    journal_update_meta_t *update_meta;
    journal_commit_t *commit;

    void *buf;
    uint32_t magic;

    /* TODO: Make stop condition dependent on number of blocks
     * as well as first empty block */
    while (!stop) {

        if (mark == header->head) {
            printf("(head) ");
        }

        if (mark == header->tail) {
            printf("(tail) ");
        }

        /* Read the next block */
        buf = read_block(f, block_number);

        /* Fun with pointers - this will take the first 32-bits
         * of the buffer which for each block type will contain
         * a magic number to identify that block type. */
        magic = *((uint32_t *) buf);

        switch (magic) {
            case JOURNAL_TRANSACTION_MAGIC:
                printf("Found transaction block (0x%x) at: %d\n", magic, block_number);

                transaction = (journal_transaction_t *) buf;

                DEBUG("Sequence number: %d\n", transaction->sequence_n);
                DEBUG("Checksum:        0x%x\n", transaction->checksum);

                if (transaction->checksum != crc32((char *) transaction)) {
                    printf("Invalid transaction block checksum - aborting\n");
                    exit(EXIT_FAILURE);
                }

                break;

            case JOURNAL_UPDATE_MAGIC:
                printf("Found update meta block (0x%x) at: %d\n", magic, block_number);

                update_meta = (journal_update_meta_t *) buf;

                DEBUG("Number of updates:   %d\n", update_meta->number_updates);
                DEBUG("Checksum:            0x%x\n", update_meta->checksum);

                if (update_meta->checksum != crc32((char *) update_meta)) {
                    printf("Invalid update meta block checksum - aborting\n");
                    exit(EXIT_FAILURE);
                }

                for (i = 0; i < update_meta->number_updates; i++) {
                    DEBUG("Update block number:   %d\n", update_meta->block_run[i]);

                    /* TODO: Verify the checksum is valid. */
                    DEBUG("Update block checksum: 0x%x\n", update_meta->checksums[i]);
                }

                /* Skip to the last update block so we reach
                 * the commit block once incremented. */
                block_number += update_meta->number_updates;
                mark += update_meta->number_updates;

                break;

            case JOURNAL_COMMIT_MAGIC:
                printf("Found commit block (0x%x) at: %d\n", magic, block_number);

                commit = (journal_commit_t *) buf;

                DEBUG("Sequence number: %d\n", commit->sequence_n);
                DEBUG("Checksum:        0x%x\n", commit->checksum);

                if (commit->checksum != crc32((char *) commit)) {
                    printf("Invalid commit block checksum - aborting\n");
                    exit(EXIT_FAILURE);
                }

                break;

            case EMPTY_BLOCK:
                /* The end of the journal is signified by an empty block */
                printf("Reached end of journal\n");
                stop = 1;

                break;

            default:
                printf("Unexpected block type (0x%x) at: %d\n", magic, block_number);
                bad_blocks++;

                /* Maybe empty blocks are an indication to stop? */
                stop = (bad_blocks == BAD_BLOCKS_LIMIT);

                break;
        }

        block_number++;
        mark++;

        free(buf);
        buf = NULL;
    }
}

int simulate_replay(FILE *f, int block_number) {
    uint32_t head;
    uint32_t tail;
    int start, done, total;
    double percent;
    journal_update_meta_t *update_meta;
    int i;
    int increment = 0;
    block_t b;

    void *buf;
    uint32_t magic;

    printf("\n================================\nSimulating replay of journal\n================================\n");

    head = header->head;
    tail = header->tail;

    if (head == tail) {
        printf("Journal up-to-date: No need to replay journal (head == tail).\n");

        return 0;
    }

    /* start will be the first block of the
     * context(s) not committed */
    start = block_number + tail;
    b = start;
    total = head - tail;
    done = 0;

    printf("First block not checkpointed: %d\n", start);
    printf("Head: %d\nTail: %d\n", head, tail);
    printf("Replaying journal...\n");

    while (tail < head) {
        increment = 1;
        percent = ((double) done / (double) total) * 100;
        printf("%.2f%%\n", percent);

        buf = read_block(f, b);
        printf("Read block from %d\n", b);
        magic = *((uint32_t *) buf);

        switch (magic) {
            case JOURNAL_TRANSACTION_MAGIC:
                break;

            case JOURNAL_COMMIT_MAGIC:
                break;

            case JOURNAL_UPDATE_MAGIC:
                /* Need to do some work */
                update_meta = (journal_update_meta_t *) buf;

                done++;
                tail++;
                b++;

                printf("Number of updates: %d\n", update_meta->number_updates);

                for (i = 0; i < update_meta->number_updates; i++) {
                    printf("Read block from %d\n", b);
                    printf("Copy update %d to block number %d\n", i, update_meta->block_run[i]);
                    tail++;
                    b++;
                    done++;
                }

                increment = 0;

                break;

            default:
                break;
        }

        if (increment) {
            done++;
            tail++;
            b++;
        }

    }

    printf("\nJournal replay complete\n");

    return 0;
}

FILE *load_image(char *path) {
    /* Loads the disk image at the specified path as a file
     * so that it can be seeked, read, etc. */

    FILE *f;

    if ((f = fopen(path, "r")) == NULL) {
        /* Failed to open file for some reason */
        printf("Error when opening file: %s\n", strerror(errno));

        exit(EXIT_FAILURE);
    }

    return f;
}

void move_to_block(FILE *f, int block_number) {
    /* Moves through the file as though it is a disk
     * by calculating positions to seek to. */

    int pos = block_number * BLOCK_SIZE;

    /* Move to the position so any reads occur at the next block */
    fseek(f, pos, SEEK_SET);

}

journal_block_header_t *read_journal_header(FILE *f, block_t block_number) {
    /* Attempts to read a journal header from the block given. If the
     * disk contains a valid JFS then the loaded header will be returned. */

    uint16_t header_magic;
    void *buf;
    journal_block_header_t *jh;

    buf = read_block(f, block_number);
    jh = (journal_block_header_t *) buf;

    /* Check if it is valid (aren't magic numbers great?!) */
    if (jh->journal_id != JOURNAL_ID) {
        printf("Invalid journal header magic found (0x%x) - aborting\n", header_magic);

        exit(EXIT_FAILURE);
    }

    DEBUG("Found valid journal header at %d\n", block_number);
    DEBUG("Block size (bits):     %d\n", jh->block_size);
    DEBUG("Journal size (blocks): %d\n", jh->total_blocks);
    DEBUG("Journal head:          %d\n", jh->head);
    DEBUG("Journal tail:          %d\n", jh->tail);
    DEBUG("First sequence number: %d\n", jh->first_sequence_n);

    return jh;
}

struct super_block *read_super(FILE *f) {
    /* Reads the superblock from the disk image loaded
     * into the file pointer given. If a valid JFS superblock
     * is found then it will be returned. */

    struct super_block *sb;

    if ((sb = malloc(sizeof(struct super_block))) == NULL) {
        printf("Failed to malloc superblock buffer\n");

        exit(EXIT_FAILURE);
    }

    /* We don't need to calculate the position, as the superblock
     * is always at the same offset (after the first 1024 bits which
     * contain the MBR) */
    fseek(f, SUPERBLOCK_OFFSET, SEEK_SET);

    fread(sb, sizeof(struct super_block), 1, f);

    /* Check the superblock loaded is valid */
    if (sb->s_magic != SUPER_JOURNAL) {
        printf("Invalid superblock magic found (0x%x) - aborting\n", sb->s_magic);

        /* If the superblock is invalid then there is probably
         * no point in continuing as it is likely this is not a JFS. */
        exit(EXIT_FAILURE);
    }

    DEBUG("Found valid superblock at offset %d\n", SUPERBLOCK_OFFSET);

    return sb;
}

void *read_block(FILE *f, int block_number) {
    /* Reads a block from the disk image at the block
     * number given */

    void *buf;

    if ((buf = malloc(BLOCK_SIZE)) == NULL) {
        printf("Failed to malloc buffer for block");

        exit(EXIT_FAILURE);
    }

    /* Seek to the position we care about before reading */
    move_to_block(f, block_number);

    fread(buf, BLOCK_SIZE, 1, f);

    return buf;
}

uint32_t crc32(char *data) {
    /* Implementation adapted from: http://www.hackersdelight.org/hdcodetxt/crc.c.txt */

   int i, j;
   unsigned int byte, crc, mask;
   static unsigned int table[256];

   /* Set up the table, if necessary. */
   if (table[1] == 0) {
      for (byte = 0; byte <= 255; byte++) {

         crc = byte;

         for (j = 7; j >= 0; j--) {    /* Do eight times. */
            mask = -(crc & 1);
            crc = (crc >> 1) ^ (0xEDB88320 & mask);
         }

         table[byte] = crc;
      }
   }

   /* Through with table setup, now calculate the CRC. */
   i = 0;
   crc = 0xFFFFFFFF;

   while ((byte = data[i]) != 0) {
      crc = (crc >> 8) ^ table[(crc ^ byte) & 0xFF];
      i = i + 1;
   }

   return ~crc;
}
