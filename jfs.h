#include <stdint.h>
#include <inttypes.h>

/* These structs and #defines are copied
 * from the journal.h include file from MINIX.
 *
 * This is done so that we can run this program
 * outside of MINIX, and it is easier to create
 * a new file with generic types, rather than
 * include all the dependencies from within
 * the MINIX source. It also has the added benefit
 * of allowing things to be slimmed down and only
 * include what is actually needed.
 */

#define JOURNAL_TRANSACTION_MAGIC 0xc3c8
#define JOURNAL_UPDATE_MAGIC      0x4eb6
#define JOURNAL_COMMIT_MAGIC      0x98f0

#define JOURNAL_ID 0x2222

#define SUPER_JOURNAL 0x6B14

#define JOURNAL_DEFAULT_CONTEXT_SIZE 32

typedef uint32_t block_t;

typedef uint32_t m_ino_t;
typedef uint16_t m_zone1_t;
typedef uint16_t m_short;
typedef uint32_t m_off_t;
typedef uint32_t m_zone_t;
typedef uint8_t m_char;
typedef uint32_t m_pointer;
typedef uint16_t m_int;
typedef uint32_t m_bit_t;
typedef block_t m_block_t;

typedef uint16_t m_dev_t;

struct super_block {
      m_ino_t s_ninodes;
      m_zone1_t  s_nzones;
      m_short s_imap_blocks;
      m_short s_zmap_blocks;
      m_zone1_t s_firstdatazone;
      m_short s_log_zone_size;
      m_short s_pad;
      m_off_t s_max_size;
      m_zone_t s_zones;
      m_short s_magic;


      m_short s_pad2;
      m_short s_block_size;
      m_char s_disk_version;

      m_pointer s_isup;
      m_pointer s_imount;
      m_int s_inodes_per_block;
      m_dev_t s_dev;
      m_int s_rd_only;
      m_int s_native;
      m_int s_version;
      m_int s_ndzones;
      m_int s_nindirs;
      m_bit_t s_isearch;
      m_bit_t s_zsearch;

      m_ino_t journal_inode;

      m_block_t journal_size;

};

typedef struct journal_block_header {

    /*
     * An ID for the journal. Currently set to a magic number for
     * this version/implementation of JFS.
     * TODO: Add more than 1 magic number spread across the journal
     * header as a means of detecting corruption.
     */
    uint16_t journal_id;

    /*
     * The size of a block in the journal. Should be the same as the
     * global block size in the superblock (i.e. of the device).
     */
    uint32_t block_size;

    /* Total blocks used by the journal (i.e. the journal size). */
    uint32_t total_blocks;

    /* A reference to the very first block used by the journal */
    block_t first;

    /* An index of the first free block which we are currently
     * on in the journal. */
    block_t head;

    /* An index to the end of the last checkpointed context */
    block_t tail;

    /* The last block in the journal */
    block_t last;

    /* The first sequence of journal entries expected. */
    uint32_t first_sequence_n;

} journal_block_header_t;

typedef struct journal_transaction {

    uint32_t transaction_id;

    uint32_t checksum;

    uint32_t sequence_n;

} journal_transaction_t;

typedef struct journal_commit {

    uint32_t commit_id;

    uint32_t checksum;

    uint32_t sequence_n;

} journal_commit_t;

typedef struct journal_update_meta {

    uint32_t update_meta_id;

    uint32_t number_updates;

    block_t block_number;

    uint32_t checksum;

    block_t block_run[JOURNAL_DEFAULT_CONTEXT_SIZE - 3];

    uint32_t checksums[JOURNAL_DEFAULT_CONTEXT_SIZE - 3];

} journal_update_meta_t;
